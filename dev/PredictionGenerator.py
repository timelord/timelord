dynamic_data_dict = {df_name : {'df': df,
                                'date_column': date_column,
                                'period_lengths': period_lengths,
                                'operations': operations,
                                'id_col': id_col}}

temp_features(**dynamic_data_dict[df_name])

#This data gets merged into one single static_data df
static_df_dict = {df_name1: df1,
                  df_name2: df2}


class PredictionGenerator(object):
    
    def __init__(self, 
                 dynamic_data_dict, 
                 dynamic_merge_args, 
                 static_df_arg_dict, 
                 static_df_merge_args,
                 final_merge_args):
        
        self.dynamic_data_dict = dynamic_data_dict      
        self.dynamic_merge_args = dynamic_merge_args      
        self.static_df_arg_dict = static_df_arg_dict         
        self.static_df_merge_args = static_df_merge_args      
        self.final_merge_args = final_merge_args
        
        self.dynamic_data = None
        self.static_data = None
        self.model_data = None


    def merge_and_set_static_data(self):
        self.static_df = work_databases(self.static_df_arg_dict, self.static_df_merge_args)
    
    def merge_and_set_dynamic_data(self, dynamic_data_dict):
        self.dynamic_data = work_databases(dynamic_data_dict)
        
    def set_model_data(self):
        self.model_data = work_databases(self.dynamic_data, 
                                         self.static_data, 
                                         self.final_merge_args)
        
    def generate_new_arg_dict(df_arg_dict, date):
        for df_name, df_arg in df_arg_dict.items():
            df = df_arg['df']
            date_column = df_arg['date_column']
            new_df = df.loc[df[date_column] <= date]
            df_arg['df'] = new_df
            df_arg_dict[df_name] = df_arg
        return df_arg_dict

    def generate_temp_data(self, df_arg_dict):
        temp_data_dict = {}
        for df_name, df_arg in df_arg_dict.items():
            temp_data_dict[df_name] = generate_temp_features(df_arg)
        return temp_data_dict    
    
    def fit_model(self, data_Xy):
        self.model = classifier().fit(data)
        
    def make_forecast(self, data):
        return self.model.predict_proba(data)
    
    def generate_model_data(self, dynamic_data_dict):
        temp_data_dict = generate_temp_data(dynamic_data_dict)
        self.merge_and_set_dynamic_data(temp_data_dict)
        self.set_model_data
    
    def gen_train_data_and_fit_model(self):
        self.merge_and_set_static_data()
        self.generate_model_data(self.dynamic_data_dict)
        self.train_model(model_data)

    def generate_forecast(self, date_range):
        self.merge_and_set_static_data()
        forecast_dict = {}
        for date in date_range:
            new_dynamic_data_dict = generate_new_arg_dict(self.dynamic_data_dict, date)
            model_data = self.generate_model_data(new_dynamic_data_dict)
            
            forecast = self.make_forecast(model_data)   
            forecast_dict[date] = forecast
        return forecast_dict
    
        