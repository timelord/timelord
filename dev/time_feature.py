from table_timeseries import TableTimeSeries
from util import Util
from collections import Counter
import pandas as pd
import time
from temporal_feature_generator import TempFeatureGenerator
import numpy as np


log_slice = pd.read_csv('..\data\churn_log_slice.csv')
member_slice = pd.read_csv('..\data\churn_member_slice.csv')
transactions_slice = pd.read_csv('..\data\churn_transactions.csv')

def get_new_dates(df, date_col):
    TTS = TableTimeSeries()
    df[date_col] = df[date_col].apply(Util.break_date)
    df = TTS.recast_date_column(df, date_col)
    return df



def calc_eta_time(total_amount, i, start_time): #UNRELATED Cool helper function
    if i > 0:
        total_duration = time.time() - start_time
        duration_per_iteration = total_duration / i
        amount_remaining = total_amount - i
        time_remaining = amount_remaining * duration_per_iteration
        return time_remaining, time.time() - start_time, total_amount, i


user_log_column_operations = {
    'num_25': ['sum', 'mean', 'nunique', 'max', 'std'],
    'num_75': ['sum', 'mean', 'nunique', 'max', 'std'],
    'num_100': ['sum', 'mean', 'nunique', 'max', 'std'],
    'num_unq': ['sum', 'mean', 'nunique', 'max', 'std'],
    'total_secs': ['mean', 'max', 'std'],
}
transaction_column_operations = {
    'payment_method_id': ['nunique'], #Also add as static variable?
    'payment_plan_days': ['mean'],
    'plan_list_price':['sum', 'mean', 'nunique'],
    'actual_amount_paid':['sum', 'mean', 'nunique'],
    'is_auto_renew': ['nunique', 'sum'],
    'is_cancel': ['nunique', 'sum'],
}

#CUSTOM
df_dict = {}

trans_slice = get_new_dates(transactions_slice.copy(), 'membership_expire_date')
user_log_slice = get_new_dates(log_slice.copy(), 'date')

df_dict['trans_df'] = {"df": trans_slice,
                       "date_col": 'membership_expire_date',
                       "id_col": 'msno',
                       "period_lengths": [30, 60, 120, 180, 360, 720, 1080],
                       "col_operations": transaction_column_operations
                      }
df_dict['log_df'] = {"df": user_log_slice,
                     "date_col": 'date',
                     "id_col": 'msno',
                     "period_lengths": [30, 60, 120, 180, 360, 720, 1080],
                     "col_operations": user_log_column_operations
                    }



TFG = TempFeatureGenerator()
train_dfs_temporal = {}
for df_key in df_dict:
    temp_feature_df = TFG.create_temp_features(df_dict[df_key]['df'], df_dict[df_key]['date_col'],
                                                df_dict[df_key]['period_lengths'], df_dict[df_key]['col_operations'],
                                                df_dict[df_key]['id_col'])
    train_dfs_temporal[df_key] = {"df": temp_feature_df,
                                  "join_col": df_dict[df_key]['id_col']
    }

print('Done with temp feature generation')


# ---------------------------------------- Make custom for eacect -------------------------------------
#Get features from the static data
dummy_columns = ['registered_via', 'gender', 'city']
#Get dummy features
dummy_data = pd.get_dummies(member_slice[dummy_columns].fillna(0).astype('str')) #Gender has NaNs
#Get timedelta as feature
member_slice = get_new_dates(member_slice, 'registration_init_time')
member_slice = get_new_dates(member_slice, 'expiration_date')
dummy_data['member_time_days'] = (member_slice['expiration_date'] - member_slice['registration_init_time']).apply(lambda x: x.total_seconds()/ 86400)
dummy_data['msno'] = member_slice['msno']

# ---------------------------------------- End custom section  ------------------------------------------------

#MERGE STUKJE


# # ---------------------- tot hier alles wat voor train gebeurt, nu voor test --------------------------------------
# #TODO: Change to dict structure
# main_df = transactions_slice
# main_date_column = 'membership_expire_date'
# period_lengths = [30, 60, 120, 180, 360, 720, 1080]
# id_col = 'msno'
# ids = main_df.loc[:,id_col].unique()
# used_dfs = [log_slice]
# date_col_names = ['date']
# # get active users
# #TODO: When going live change to datetime.datetime.now().strftime("%Y-%m-%d)
# current_date = pd.to_datetime('2017-01-01')
#
# #Obtain active ids from the main df
# new_main_slice = get_new_dates(main_df.copy(), main_date_column)
# #A customer is still active when his expire date exceeds the "Current date"
# active_ids = list(map(lambda x: x[0], Counter(new_main_slice.loc[new_main_slice.loc[:, main_date_column] >
#                                                                       current_date].loc[:, id_col]).most_common()))
# active_ids_main = new_main_slice.loc[new_main_slice.loc[:, id_col].isin(active_ids)]
#
# #get all active users out of the other dataframes
# active_id_dfs = [active_ids_main]
# for ix, df in enumerate(used_dfs):
#     new_df_slice = get_new_dates(df.copy(),date_col_names[ix])
#     active_id_df = new_df_slice.loc[new_df_slice.loc[:, id_col].isin(active_ids)]
#     active_id_dfs.append(active_id_df)
#
# # loop over prediction dates:
# #   slice to prediction date
# date_col_names = [main_date_column] + date_col_names
# active_id_period_dfs = []
# prediction_dates = ["2016-11-21", "2016-12-31"]
# for date in prediction_dates:
#     for ix, df in enumerate(active_id_dfs):
#         active_id_period_df = df.loc[(df[date_col_names[ix]] < pd.to_datetime(date))]
#         active_id_period_dfs.append(active_id_period_df)
# print('Done with testdata prep')