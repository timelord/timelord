[![Build status](https://gitlab.com/timelord/timelord/badges/master/build.svg)](https://gitlab.com/timelord/timelord/commits/master)


# Timelord v0.1

A project that aims to simplify working with time related data by exposing the following core functionality:

- `LagFeatureAggregator`: Creating lag features based on time stamped data
- `TableTimeSeries`: Creating complete time series based on time stamped data
- `MultiVarForecast`: Preprocess data to prepare it for multi-variate time series models
- `Benchmark`: Benchmarking time series forecasts with simple time series models.
- `ForecastGenerator`:Generate a historical probability forecast for extrapolation purposes.

While providing additional functionality through the utilities:

- `RedisDataFrame`: Getting and setting pandas dataframes in Redis.
- `MergeDataFrames`: Merge multiple dataframes / tables and account for duplicate column names.


Further info about the algorithm can be found **[here](https://gitlab.com/timelord/timelord/blob/master/docs/multi-step-algorithm-information.md)**

## Install

Clone the repo and install from there on, or simply use:

```
pip install git+https://gitlab.com/timelord/timelord.git
```

## Background
This project was started to allow extrapolating a probabilty forecast of a classifier model from scikit-learn based on multi-variate time series forecasting. It has been set-up in a way that each block can be used seperately but when used in concert it can provide a probabilty time series forecast for a binary label, provided that the data is mostly of a temporal nature.

To read more about the algorithm that supposedly generates such a forecast, you can read more about it [here](http://dead_link)


### Design philosophy

The project is set-up to expand the  scikit-learn API, thus by using the `fit`, `transform`, `predict` functionality used widely within scikit-learn. Even though the API is almost identical Timelord assumes pandas dataframes are used within the Transformer & Prediction modules.
With Dask-ml also implementing the scikit-learn API it allows using most of the Timelord functionality within the Dask framework as well by swapping the Pipeline and GridSearchCV modules of scikit-learn with Dask-ml.

The goal of Timelord is to expand the scikit-learn functionality and bridge the differences between Pandas, scikit-learn and statsmodels on the area of time series.

## Usage

Beneath are some brief examples of the core & utils functionality of Timelord. 
For more details you can view the respective notebooks.


### LagFeatureAggregator 
A module that enables the creation of temporal features to allow a classifier model to use dynamic data as features. 
Necessary arguments are an ID column, a Date column, a period_lenght and a dictionary with operations per column. It then makes a slice of N lags long, executes the dictionary with operations and repeats this process for as many lags as there are before returning a concatted dataframe with the calculated features.


```python
date_column = 'date'
id_col = 'user'
period_lengths = [10, 30]
column_operations = {
    'muscle_growth': ['max', 'median', 'min']
}

temp_feat_gen = LagFeatureGenerator(date_column, period_lengths, column_operations, id_col, max_date=None, njobs = -1)
temp_feats = temp_feat_gen.fit_transform(df)
```
For more information view the examples between chapter 1 and 2 written here:
https://gitlab.com/timelord/timelord/blob/master/examples/descriptive_pipeline_example.ipynb 

### TableTimeSeries 
This module focuses on creating only 'complete' time series dataframes. Such that each timestamp occurs uniquely and the time series is continous for the respective frequency.
It does so by joining the original dataframe against a date range based on the date column in the original dataframe and resampling this to the desired frequency, without filling / imputing / extrapolating

```python
from pandas as pd
from timelord.table_timeseries import TableTimeSeries

TTS = TableTimeSeries(
    date_column = 'dates',
    freq = '3H',
    **kwargs
)
tdf = TTS.fit_transform(EXAMPLE_DF)
```

For more info view: https://gitlab.com/timelord/timelord/blob/master/examples/TableTimeSeries-example.ipynb

### MultiVarForecast

This module contains several utility objects that aid in the development of multivariate time series forecasting pipelines. It contains several scikit-learn like transformers (fit / transform / inverse_transform) and a VAR (optional) model written around a Regressor implementation of scikit-learn.
A module that uses the Statsmodels VAR model and adds multiple transform and preperation utility functions around it to ease the use of the model.


```python
from timelord.datasets.load_data import load_grocery
from timelord.multivar_forecast import DropPerfectCorrelation, LogTransform, DropConstants, MultiVarForecast
from sklearn.pipeline import Pipeline

MVF = MultiVarForecast(
                model_kwargs = {}, 
                fit_kwargs = {'maxlags': 14}
                predict_kwargs = {'forecast_length': 14} 
                )

pipe = Pipeline(steps=[
    ('drop_corr', DropPerfectCorrelation()),
    ('drop_const', DropConstants()),
    ('log_transform', LogTransform()),
    ('VAR_predict', MVF)
])


df = load_grocery().long_multi_var_data #load some example data
prediction = pipe.fit_predict(df) #Returns a dataframe with the forecast
```

For more information and examples see the notebook below:

https://gitlab.com/timelord/timelord/blob/master/examples/MultiVarForecast-example.ipynb 

### Benchmark
This module is used to evaluate the succes of a time series forecast by also forecasting using some simple time series methods. The idea being that the exotic model might be inferior to a simplistic model like a Seasonal Naive. It cuts off a small percentaqge off the time series data to use the remainder for evaluation purposes.

```python
from timelord.datasets.load_data import load_grocery
df = load_grocery().long_multi_var_data
df = df[df.index < "2017-8-16"] # removes the trailing zeros
ts = pd.DataFrame(df['all_products_all_stores_unit_sales'])

# Create the benchmark
bench = Benchmark()
bench.benchmark(ts, forecast_length = 14, seasonal_period = 7)
```

### ForecastGenerator
The module that creates a prediction probability time series as seed for the time series model for extrapolation. By iterating over the dynamic data and generating new temporal features and build a forecast from that.
For further information see this entire notebook as example:

```python
#Brief example upcoming
```
https://gitlab.com/timelord/timelord/blob/master/examples/descriptive_pipeline_example.ipynb


### DataFrameRedis
This module aids in setting and getting a pandas Dataframe into a redis database as a value. Note that it does so as a _MessagePack_ and thus chunks the dataframe.

Assuming you have a Redis database running locally on port 6379 (default port) you can use the following code:

Example:

```python
import pandas as pd
from timelord.utils.redis_dataframe import DataFrameRedis

DFR = DataFrameRedis()
empty_list = DFR.get_df('test_dataframe') #Returns a list because nothing is set.

df = pd.DataFrame([[1, 2]])
DFR.set_df('test_dataframe', df) #The dataframe is set
DFR.get_df('test_dataframe) #returns the original dataframe.
```

For more info view: https://gitlab.com/timelord/timelord/blob/master/examples/DataFrameRedis-example.ipynb

### MergeDataFrames
A utility class that helps in merging several dataframes with shared column names (usual in normalized databases). Can also be used to merge Dask Dataframes. Some base functionality is to return a dataframe with newly named columns (by mapping), providing a callback before the joining occurs. This class is only used when joining more than 3 dataframes with each other, otherwise regular joining by the default pandas methods is more tidier.


```python
#Build the data
size = 10
df1 = pd.DataFrame(np.random.random((size, 3)), columns = ['A', 'B', 'C'])
df2 = pd.DataFrame(np.random.random((size, 3)), columns = ['B', 'C', 'D'])
df3 = pd.DataFrame(np.random.random((size, 3)), columns = ['H', 'I', 'J'])

df1['join'] = np.arange(size)
df2['join'] = np.arange(size)

df2['not_shared_join'] = np.arange(size)
df3['not_shared_join'] = np.arange(size)

df1['equal'] = np.arange(size)
df2['equal'] = np.arange(size)
df3['equal'] = np.arange(size)

#Create arguments for the class
df_dict = {}
df_dict['df1'] = df1.copy()
df_dict['df2'] = df2.copy()
df_dict['df3'] = df3.copy()

def add_100_callback(df):
    df['B'] += 100
    return df

pre_join_callback_map = {'df1': add_100_callback}

keep_columns2 = {'df1': ['A', 'B', 'equal'],
               'df2': ['B', 'D', 'equal', 'not_shared_join'],
               'df3': ['H', 'equal', 'not_shared_join']}

df_relations = [(('df1', 'join'), ('df2', 'join'), 'left'), #df1 <> df2 && df2 <> df3 (different key)
                 (('df2', 'not_shared_join'), ('df3', 'not_shared_join'), 'left')]

#Call the class
mdf = MergeDataframes(df_dict = df_dict,
                    base_df_name = 'df1',
                    df_relations = df_relations,
                    keep_columns = new_keep_columns,
                    pre_join_callback_map = pre_join_callback_map)

mdf.merge_relations()
final_df = mdf._base_df

```


### Basic example complete pipeline for illustration purposes:

<Add notebook / complete pipe example>
An example notebook demonstrating all the modules linked to create a time series probability forecast:

https://gitlab.com/timelord/timelord/blob/master/examples/complete_pipe_proof_of_concept_original_DEPRECATED.ipynb

### Contributing
Source code and bug tracker are on github: https://gitlab.com/-/ide/project/timelord/timelord/

To run tests, install "pytest" Python package and then run pytest command from the source checkout.

Look at the issues https://gitlab.com/timelord/timelord/issues to see if there's one for you. Create your feature branch, write your feature and make a PR.


### Change log

##### V0.1 --> V0.2 (Expected mid July 2018):


- Every module in the repo is rewritten to extend the sklearn transformer API, the regressor extends the RegressorMixin to conform to the sklearn API.
- Rewrote & renamed the TableTimeSeries module into CompleteTimeSeries and made a transformer out of it. Also remove a lot of base logic with the invocation of `.resample`.
- Rewrote LagFeatureGenerator to utilize the Parallel function for a speed increase in computation.
- Broke up the MultiVarForecast into seperate transformers and a Regressor that allows different statsmodels regressor models.
- Added MergeDataFrames to utils to easily merge multiple dataframes with shared column names
- Added a Benchmark class to evaluate the performance of the MultiVarForecast
- Expanded the Readme with more relevant information
- Created a ReadTheDocs page
- Added an article going deeper on co-integration within multi-variate time series.
- Added more tests & inline documentation
- and more

### License and Author


- Author:: Casper Lubbers casper.lubbers@kpn.com

- Author:: Celeste Kettler celeste.kettler@kpn.com

- Author:: Erdal Yildiz erdal.yildiz@kpn.com

- Author:: John Ciocoiu john.ciocoiu@kpn.com

- Author:: Rudy Veenhoff rudy.veenhoff@kpn.com

- Author:: Martin Tjon martin.tjon@kpn.com

- Copyright 2017-2018, KPN ICT Consulting

Licensed under the GNU General Public License v3.0; you may not use this project except in compliance with the License.