# Contributing

You have come this far, great!

## Creating an issue
You have a great idea for a new use case for timelord , or a problem you want fixed.

Please also have a look at the open issues:
https://gitlab.com/timelord/timelord/issues

And add a new issue if needed.

To run tests, install "pytest" Python package and then run pytest command from the source checkout.

Look at the issues https://gitlab.com/timelord/timelord/issues to see if there's one for you. 
Create your feature branch, write your feature and make a PR.