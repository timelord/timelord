# Feature Request

Please explain your feature request by giving your user story: 'As a ... I want to ... so I ...'.

# Suggested Implementation

If you can, please provide us with a suggested implementation of your feature.

# Example Project

If you can, please create an example project on gitlab.com and link it here so we can look at the implementation.

/label ~feature
/cc @project-manager
