# Requirement

Specify the product requirement and why it's necessary.

# Suggested implementation

If you can, specify how the requirement could be implemented.

## Impact

Specify what files will be affected and how the requirement relates to existing issues.

/label ~requirement
/cc @project-manager
