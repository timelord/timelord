import numpy as np
from sklearn.base import TransformerMixin, BaseEstimator
from sklearn.exceptions import NotFittedError

class InterpolateTransformer(TransformerMixin, BaseEstimator):
    name = "InterpolateTransformer"

    def __init__(self, interpolate_kwargs = None):
        self.interpolate_kwargs = interpolate_kwargs

    def transform(self, X):
        """
        Transform the dataframe to something interpolate

        Parameters
        ----------
        X : pandas.DataFrame
            The DataFrame to transform

        Returns
        ----------
        pandas.DataFrame
            The transformed dataframe.

        """
        if self.interpolate_kwargs is not None and isinstance(self.interpolate_kwargs, dict):
            X = X.interpolate(**self.interpolate_kwargs)

        return X

    def fit(self, X, y=None):
        """
        Nothing to fit

        Parameters
        ----------
        X : pandas.DataFrame
            The DataFrame to fit

        Returns
        ----------
        pandas.DataFrame
            The fitted dataframe.

        """
        return self

    def inverse_transform(self, X):
        """
        Nothing to inverse

        Parameters
        ----------
        X : pandas.DataFrame
            The DataFrame to inverse

        Returns
        ----------
        pandas.DataFrame
            The inversed dataframe.
        
        """
        return X
