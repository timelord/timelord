from joblib import Parallel, delayed
from timelord.lag_feature_aggregator import LagFeatureAggregator as lfa
import pandas as pd


def mp_func(arg):
    lfgenerator, group = arg
    return lfgenerator.fit_transform(group)


class LagFeatureGenerator(object):
    def __init__(self, date_column, lags, ops, id_col, max_date, njobs=1):
        self.date_column = date_column
        self.lags = lags
        self.ops = ops
        self.id_col = id_col
        self.max_date = max_date
        self.njobs = njobs
        self.lag_feature_map = {}

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        lfaggregator = lfa(self.date_column, self.lags, self.ops, self.id_col, self.max_date)
        id_groups = X.groupby(self.id_col)
        x = Parallel(n_jobs=self.njobs)(delayed(mp_func)((lfaggregator, group)) for name, group in id_groups)
        lag_features_df = pd.concat(x)
        return lag_features_df


