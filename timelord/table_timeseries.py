import time #Only used to time the duration
import pandas as pd

from sklearn.base import TransformerMixin, BaseEstimator
from sklearn.exceptions import NotFittedError


class TableTimeSeriesTransformer(TransformerMixin, BaseEstimator):
    """
    Converts a timestamped dataframe into a complete time series based on a provided resolution.
    Provided that the timestamp is in a Datetime object or pandas Timestamp object it does the following:
    
    1) Set the date-column as index
    2) Call .resample with the frequency argument
    3) Call the resample object with a certain operation (if set)
    4) Return dataframe
    
    """
    DEBUG=False

    name = "TableTimeSeriesTransformer"
    
    def __init__(self, date_column="index", min_freq="1D", operations="asfreq", resample_kwargs = None):
        """
        Create a TTS object with the given parameters.

        Parameters
        ----------
        df : DataFrame
            The DataFrame that is used for the time series
        
        date_column : string
            The name of the column that has the dates
        
        min_freq : string
            Accepts the arguments: 'year', 'month', 'day', 'hour', 'minute', 'second'
         
        operations : string or Dict
            (default = 'asfreq') If a dict is provided with a mapping for each column this will be forwarded and called with a .agg method for the respective columns. Unmapped columns _will_ be dropped silently.

        interpolate_kwargs : Dict
            (default = None) The kwargs that will be used as arguments for the `.interpolate()` method. Is skipped by default.
        
        fillna args : value
            (default = None) The value that will be used to fill the NaNs with.
        """
        
        self.date_column = date_column
        self.min_freq = min_freq
        self.operations = operations
        self.resample_kwargs = resample_kwargs
    
    def fit(self, X, y=None):
        """
        Nothing to fit

        Parameters
        ----------
        X : pandas.DataFrame
            The DataFrame to fit

        Returns
        -------
        pandas.DataFrame
            The fitted dataframe.

        """
        return self

    def inverse_transform(self, X):
        """
        Nothing to inverse

        Parameters
        ----------
        X : pandas.DataFrame
            The DataFrame to inverse

        Returns
        -------
        pandas.DataFrame
            The inversed dataframe.
        
        """
        return X

    def transform(self, df): 
        """
        Creates a DataFrame with only the new date range as index.
        The original DataFrame will be merged against that resulting in a DataFrame
        with a lot of empty values.
        
        Parameters
        ----------
        df : pandas.DataFrame
            The DataFrame that is used for the time series
        
        Returns
        -------
        pandas.DataFrame
            A new DataFrame object with the index as a complete DatetimeIndex and the gaps as NaN's and NaT's for further refinement.
        
        """

        new_df = df
        new_df = new_df.set_index(self.date_column)
        
        if self.resample_kwargs is not None and isinstance(self.resample_kwargs, dict):
            new_df = new_df.resample(self.min_freq, **self.resample_kwargs)
        else:
            new_df = new_df.resample(self.min_freq)

        if self.operations == 'asfreq':
            new_df = new_df.asfreq()
        elif isinstance(self.operations, dict):
            new_df = new_df.agg(self.operations)
        else:
            raise TypeError("'operations' has to be a dict")

        return new_df        
    
class JoinTimeSeriesDataFrames():
    def handle_multiple_dates(self, time_df, column = 'join_column_name', operations = 'sum'):
        """
        WARNING: Needs more refinement. Do not use.
        Is this even useful here? Maybe a bit overkill?
        """      
        if operations:
            new_df = time_df.groupby(column).agg(operations)
        # if self.DEBUG:
        #     print("handle_multiple_dates: Dropped columns: {} with operations: {}", format(time_df.shape[1] - new_df.shape[1]), operations)
        return new_df
    
    def join_time_series_dfs_on_index(self, df1, df2, join_column_name = 'join_column_name', set_as_index = False, min_date = None, max_date = None):
        #Assumes that the time series are in the index
        #Assumes that the time series are complete from begin to end
        #Assumes that they are in the same frequency
        #Assumes that the wish is to extend the date ranges with both the extremes.
        df1[join_column_name] = df1.index
        df2[join_column_name] = df2.index
        
        if min_date:
            df1 = df1.loc[df1.index >= min_date]
            df2 = df2.loc[df2.index >= min_date]
        if max_date:
            df1 = df1.loc[df1.index <= max_date]
            df2 = df2.loc[df2.index <= max_date]
            
        first_date = self.__get_first_date(df1, df2)
        last_date = self.__get_last_date(df1, df2)
        new_date_range = pd.date_range(start = first_date, end = last_date, freq = df1.index.freq, dtype = 'datetime64[d]')
        #Construct the new dataframe to merge against 
        #NOTE: Maybe break apart?
        new_tdf = pd.DataFrame(new_date_range, columns = [join_column_name])
        new_tdf = new_tdf.merge(right = df1,
                                on = join_column_name,
                                how = 'left')

        new_tdf = new_tdf.merge(right = df2,
                                on = join_column_name,
                                how = 'left')
        if set_as_index:
            new_tdf.index = pd.DatetimeIndex(new_tdf[join_column_name], freq = df1.index.freq)
            new_tdf = new_tdf.drop(join_column_name, axis = 1)
        return new_tdf
                
    def __get_first_date(self, df1, df2):
        if df1.index[0] > df2.index[0]:
            first_date = df2.index[0]
        else:
            first_date = df1.index[0]
        return first_date

    def __get_last_date(self, df1, df2):
        if df1.index[-1] > df2.index[-1]:
            last_date = df1.index[-1]
        else:
            last_date = df2.index[-1]
        return last_date
