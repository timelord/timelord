# -*- coding: utf-8 -*-
# Part of Timelord. See LICENSE file for full copyright and licensing details.

"""Project metadata

Information describing the project.
"""

# The package name, which is also the 'UNIX name' for the project.
package = 'timelord'
project = 'A framework to easily transform and manipulate timestamped data into timeseries and temporal features.'

#version = '2016.02.23'  #date versioning
version = "2017.12.15"

description = 'A framework to easily transform and manipulate timestamped data into timeseries and temporal features.'

authors = [ 'Casper Lubbers',
            'Erdal Yildiz',
            'Celeste Kettler']
authors_string = ', '.join(authors)
emails = [ 'erdal.yildiz@kpn.com',
           'casper.lubbers@kpn.com',
           'celeste.kettler@kpn.com',
           'john.ciocoiu@kpn.com']
license = 'GPL-3.0'
copyright = '2018 ' + authors_string
url = 'https://gitlab.com/timelord/timelord'

