import numpy as np
import pandas as pd
from collections import defaultdict

from statsmodels.tsa.api import VAR
from statsmodels.tsa.stattools import adfuller, kpss
import pandas as pd
import numpy as np

from statsmodels.tsa.api import VAR
from sklearn.base import RegressorMixin, BaseEstimator, TransformerMixin
from sklearn.exceptions import NotFittedError


#Unobserved Components
#SARIMAX
#VARMAX
#

class ExpandedTransformerMixin(TransformerMixin):
    
    def fit(self, X, y=None):
        return self
    
    def transform(self, X):
        return X
    
    def inverse_transform(self, X):
        return X
        
        
class DropPerfectCorrelation(ExpandedTransformerMixin):
    name = "DropPerfectCorrelation"

    """ 
    Iterates over each column to subtract the largest value and smallest value from each other.
    If this value equals zero than the entire column is a constant on which the model will raise
    an error. This method returns the index where this occurs.


    Parameters
    ----------
    df : pandas.DataFrame

    Returns
    -------
        pandas.DataFrame
    """       
        
    def fit(self, X, y=None):
        x_ar, y_ar = np.where(X.corr().values == 1)
        self.keep_columns = []
        self.drop_columns = []
        for (x, y) in zip(x_ar, y_ar):
            if x not in self.keep_columns and x not in self.drop_columns:
                self.keep_columns.append(x)
                self.drop_columns.append(y)
            elif y not in self.drop_columns:
                self.drop_columns.append(y)
        self.drop_columns = [X.columns[i] for i in self.drop_columns]
        self.keep_columns = [X.columns[i] for i in self.keep_columns]
        return self
    
    def transform(self, X):
        if hasattr(self, 'keep_columns'):
            X = X[self.keep_columns]
        else:
            raise NotFittedError(" {self.name} hasn't been fitted yet.")
        return X
    
class LogTransform(ExpandedTransformerMixin):
    name = "LogTransform"

    def fit(self, X, y=None):
        self.before_log_correction_val = abs(min(X.min().values)) + 2 #+1 seems to tricky
        return self
    
    def transform(self, X):
        """ 
        Sets and applies the before_log_correction before log transforming the data.
        
        Parameters
        ----------
        df : pandas.DataFrame
            The input data, which often is self.datetime_index_df
        
        Returns
        -------
        pandas.DataFrame
        """
        if self.before_log_correction_val:
            X += self.before_log_correction_val
            X = np.log(X)
        else:
            raise NotFittedError(" {self.name} hasn't been fitted yet.")
        return X
    
    def inverse_transform(self, X):
        """ 
        undoes the log transform by raising the values against an exponent and undoing the before_log_transform
        as to get back to the original values.
                
        Parameters
        ----------
        array : numpy.array, pandas.DataFrame
            The input data, which often is self.datetime_index_df
        
        Returns
        -------
        numpy.array, pandas.DataFrame
        """    
        if hasattr(self, "before_log_correction_val"):
            X = np.exp(X) - self.before_log_correction_val
        else:
            raise NotFittedError(" {self.name} hasn't been fitted yet.")
        return X
    
        
    
class DropConstants(ExpandedTransformerMixin):
    name = "DropConstants"
    """ 
    Iterates over each column to subtract the largest value and smallest value from each other.
    If this value equals zero than the entire column is a constant on which the model will raise
    an error. This method returns the index where this occurs.


    Parameters
    ----------
    df : pandas.DataFrame

    Returns
    -------
    pandas.DataFrame
    """       
    def fit(self, X, y=None):
        self.constant_columns = np.where(df.apply(lambda x: x.max() - x.min()).values == 0)[0]
        return self
    
    def transform(self, X):
        if hasattr(self, 'constant_columns'):
            X = X.drop(self.constant_columns, axis = 1)
        else:
            raise NotFittedError(f" {self.name} hasn't been fitted yet.")
        return X
    
    def inverse_transform(self, X):
        return X

class MultiVarForecast(BaseEstimator, RegressorMixin, ExpandedTransformerMixin):
    
    name = "MultiVarForecast"
    def __init__(self, model_kwargs, fit_kwargs, predict_kwargs, model = VAR):
        self.model_kwargs = model_kwargs
        self.fit_kwargs = fit_kwargs
        self.predict_kwargs = predict_kwargs
        self.model = model
        
    def _check_type(self, X):
        if type(X) != pd.DataFrame:
            raise ValueError("Expected a Pandas DataFrame, got a {}".format(type(X)))        
        
    def _build_df(self, X, y_pred, forecast_length):
        freq = X.index.inferred_freq
        new_index = pd.date_range(start = X.index[-1],
                                   freq = freq,
                                   periods = forecast_length) 
        y_pred_df = pd.DataFrame(y_pred, 
                                 columns = X.columns,
                                 index = new_index)
        return y_pred_df        
        
    def fit(self, X, y=None):
        self._check_type(X)
        self.model = self.model(X, **self.model_kwargs)
        self.results = self.model.fit(**self.fit_kwargs)
        return self
    
    def predict(self, X):
        self._check_type(X)
        lag_order = self.fit_kwargs['maxlags']
        forecast_length = self.predict_kwargs['forecast_length']
        y_pred = self.results.forecast(X.values[-lag_order:], 
                                        forecast_length)
        
        y_pred_df = self._build_df(X, y_pred, forecast_length)
        return y_pred_df
    

    def test_stationarity_columns(self, df, use_kpss = True, use_adfuller = False):
        """
        mVarForecast util
        Adfuller takes approx 100x as long as kpss
        by default only returns p_val
        stationairity with kpss if p_val is >.05
        stationarity with adfuller if p_val is <.05
        Doesnt seem necessary in general? If it is, todo : write function to filter below threshold values.

        Parameters
        ----------
        datetime_index_df : pandas.DataFrame
                
        Returns
        -------
        pandas.DataFrame
        """
        stationarity_dict = defaultdict(dict)
        for col in df.columns:
            if use_kpss:
                val = kpss(df[col])
                stationarity_dict[col]['kpss'] = val[1]
            if use_adfuller:
                val = adfuller(df[col])
                stationarity_dict[col]['adf'] = val[1]
        return stationarity_dict
        
def cut_off_data_from_start_fc_column(datetime_index_df):
    """
    WARNING: Unpredictable results as of yet. Do not use.
    
    
    Cuts off the entire dataset based on where the variance in the to-forecast column starts.
    This can be useful for dataframes where the variance starts very late on and the model
    has a hard time fitting based on a long series of zero values.
    WARNING: Needs testing in practice to test performance!
    
    Parameters
    ----------
    datetime_index_df : pandas.DataFrame
             
    Returns
    -------
    pandas.DataFrame
    """
    series = self.datetime_index_df[self.fc_column]
    ts_index_var_start = self.get_ts_index_var_start(series)
    cut_off = int(ts_index_var_start * self.cut_off_data_from_start_fc_column_percentage)
    return datetime_index_df.iloc[cut_off:, :]
