""" collection of util functions """

import pandas as pd

class Util():
    
    @classmethod
    def recast_date_column(self, df, date_column): #Move to utils?
        df = df.assign(**{date_column: pd.DatetimeIndex(df[date_column])}) #Resolve copy / slice warning
        return df
    
    @classmethod
    def map_dtype_func_map(cls, df, dtype_func_map):
        """Expects a dataframe and returns a dictionary with a function (string or function) mapped to each column based on the dtype.
        This function can be used to swiftly create a function mapping to handle double_dates in the TableTimeSeries.
        
        .. note:: It's advisable the dtype_funcmap covers each unique dtype in the dataframe, else not every column name has a mapping and it gets dropped silently in the TableTimeSeries column_operations   
        
        Examples
        --------
        >>> def agg_func(x):
        >>>    return list(x)

        >>> dtype_func_map = {'int': 'sum',
        >>>    'float': 'sum',
        >>>    'object': agg_func,
        >>>    'datetime': 'unique'}
        
        Parameters
        ----------
        df : DataFrame
            The DataFrame that is used to map the functions.
        
        dtype_func_map : dictionary
            The dictionary with the functions as value for each dtype that the dataframe will have.
            Does not have to be a 1-1 match, as long as the columns you desire are covered.
        
        Returns
        -------
        dictionary
            A function mapping for each column.
        
        """
        func_map = {}
        for col, dtype in zip(df.columns, df.dtypes):
            for key in dtype_func_map:
                if key in str(dtype):
                    func_map[col] = dtype_func_map[key]
        return func_map
            
