import numpy as np
import pandas as pd
from timelord.lag_feature_generator import LagFeatureGenerator
from timelord.utils.generate_labels import LabelGenerator
import timelord.utils.treat_missing_data as tmd
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score
import pickle as pkl
import glob
import os
import sys
import datetime as dt

class PredictionGenerator(object):
    """The prediction generator creates one big dataframe out of the static and dynamic data and uses this dataframe to
    generate predictions on the specified label
    
    Parameters
    ----------
    dynamic_data_dict : dict
        dictionary containing the dynamic data and some characteristics of this dataframe
    dynamic_merge_args : dict
        dictionary containing the merge arguments needed to perform the merge between the dataframes containing dynamic data
    static_df_arg_dict : dict
        dictionary containing the static data and some characteristics of this dataframe
    static_df_merge_args : dict
        dictionary containing the merge arguments needed to perform the merge between the dataframes containing static data
    final_merge_args : dict
        dictionary containing the merge arguments needed to perform the merge between the dataframes containing static data and dynamic data to form one dataframe containing all data
    label_name : string
        column in dataframe representing the label that we would like to predict
    """
    def __init__(self,
                 dynamic_data_dict,
                 dynamic_merge_args,
                 static_df_arg_dict,
                 static_df_merge_args,
                 final_merge_args,
                 print_nan_values=False):

        self.dynamic_data_dict = dynamic_data_dict
        self.dynamic_merge_args = dynamic_merge_args
        self.static_df_arg_dict = static_df_arg_dict
        self.static_df_merge_args = static_df_merge_args
        self.final_merge_args = final_merge_args

        self.dynamic_data = None
        self.static_data = None
        self.model_data = None
        self.label_df = None

        self.generated_label_name = 'churn_label'
        self.current_dynamic_data_dict = {}
        #The columns that must be dropped before the X and y for the model are made
        self.drop_cols = final_merge_args['on']

        self.row_nan_percentage_dict = None
        self.column_nan_percentage_dict = None
        self.print_nan_values = print_nan_values
        self.feature_names = None

    def gen_train_data_and_fit_model(self, date):
        """ Merges static and dynamic data, treats missing data and trains a model on this data. This model is saved in the 
        '../models/' directory
        
        Parameters
        ----------
        date: date
            End date for the periods in pd.DateTime format
        """
        # defines the currently used dynamic data dict for temporal feature generation, using all instances of the dynamic data
        self.current_dynamic_data_dict = self.dynamic_data_dict
        t_start = dt.datetime.now()
        self.merge_and_set_static_data()
        t_end = dt.datetime.now()
        print("Merging static data took: %s" %str(t_end-t_start), flush=True)
        t_start = dt.datetime.now()
        self.construct_labels(date)
        t_end = dt.datetime.now()
        print("Constructing labels took: %s" %str(t_end-t_start), flush=True)
        t_start = dt.datetime.now()
        self.generate_model_data()
        t_end = dt.datetime.now()
        print("Generating model data took: %s" %str(t_end-t_start), flush=True)
        t_start = dt.datetime.now()
        self.missing_data_treatment()
        t_end = dt.datetime.now()
        print("Missing label treatment took: %s" %str(t_end-t_start), flush=True)
        t_start = dt.datetime.now()
        self.train_model()
        t_end = dt.datetime.now()
        print("Training model took: %s" %str(t_end-t_start), flush=True)

    def merge_and_set_static_data(self):
        """ Merges the different dataframes contained in the self.static_df_arg_dict and puts the resulting dataframe
        in self.static_data
        """
        #In test phase there is only one static df, in live phase there are more whose merge should
        #be handeled by work_databases
        # self.dynamic_data = work_databases(self.static_df_merge_args)
        for i,key in enumerate(self.static_df_arg_dict):
            print('in static merger')
            static_data_df = self.static_df_arg_dict[key]
            if i == 0:
                self.static_data = static_data_df
            else:
                self.static_data = self.static_data.merge(static_data_df, how=self.static_merge_args['how'],
                                        on=self.static_merge_args['on'])
            print(self.static_data.head(3))
            print(self.static_data.shape)


    def generate_model_data(self):
        """ Creates temporal features from the dynamix data and merges the dataframes containing dynamic data into one
        after which it is merged together with the static data into one final dataframe.
        """
        temp_data_dict = self.generate_temp_data()
        self.merge_and_set_dynamic_data(temp_data_dict)
        self.set_model_data()

    def generate_temp_data(self):
        """ Makes temporal features for the dataframes that contain dynamic data and stores the new dataframe under its
        name in temp_data_dict
        
        Returns
        -------
        dict
            dictionary containing the dataframes with temporal features
        """
        TFG = LagFeatureGenerator()
        temp_data_dict = {}
        for df_name, df_dict in self.current_dynamic_data_dict.items():
            temp_data_dict[df_name] = TFG.create_temp_features(df_dict['df'], df_dict['date_col'],
                                               df_dict['period_lengths'], df_dict['col_operations'],
                                               df_dict['id_col'])
        return temp_data_dict

    def merge_and_set_dynamic_data(self, temp_data_dict):
        """ Merges the dataframes containing temporal features into one dataframe called self.dynamic_data
        
        Parameters
        ----------
        temp_data_dict : dict
            dictionary containing the dataframes with dynamic data (in temporal features)

        """
        #This function should eventually exist of only work_database functions, the functionality written
        #is only meant as temporary substitute of the functionality in work_databases
        for i,key in enumerate(temp_data_dict):
            dynamic_data_df = temp_data_dict[key]
            if i == 0:
                self.dynamic_data = dynamic_data_df
            else:
                self.dynamic_data = self.dynamic_data.merge(dynamic_data_df, how=self.dynamic_merge_args['how'],
                                        on=self.dynamic_merge_args['on'])
        #self.dynamic_data = work_databases(dynamic_data_dict)

    def set_model_data(self):
        """ Merges the dataframes containing dynamic and static data into one dataframe: self.model_data
        """
        # This function should eventually exist of only work_database functions, the functionality written
        # is only meant as temporary substitute of the functionality in work_databases
        self.model_data = self.dynamic_data.merge(self.static_data, how=self.final_merge_args['how'],
                                                  on=self.final_merge_args['on'])
        if self.label_df is not None:
            self.model_data = self.model_data.merge(self.label_df, how='left',
                                                    on='msno')
        print('is merged')
        #self.model_data = work_databases(self.dynamic_data,
        #                                 self.static_data,
        #                                 self.final_merge_args)


    def missing_data_treatment(self):
        """ Eliminates or replaces the missing values so that a dataframe is returned containing only cells with value,
        which is put back into self.model_data
        """
        self.model_data, self.row_nan_percentage_dict, self.column_nan_percentage_dict = tmd.treat_missing_data(self.model_data)
        if self.print_nan_values:
            for row, percentage in self.row_nan_percentage_dict.items():
                print('row: %s , percentage nans: %f' % (row, percentage))
            for col, percentage in self.column_nan_percentage_dict.items():
                print('column name: %s , percentage nans: %f' % (col, percentage))

    def save_model(self, model, model_name):
        """Save the created model in the models directory
        
        Parameters
        ----------
        model : pickle.model
            trained machine learning model
        model_name : string
            name of the machine learning model in string format
        """
        current_datetime = pd.to_datetime('now')
        filename = "../models/model_" + model_name + "_" + str(current_datetime).replace(":", "_") + ".sav"
        pkl_file = open(filename, 'wb')
        pkl.dump(model, pkl_file)
        pkl_file.close()

    def train_model(self):
        """ Splits the dataframe (self.model_data) in an X and a y, where X are the features that are used to predict y.
        The resulting model is saved in ../models/
        """
        y = self.model_data[self.generated_label_name].values
        X = self.model_data.drop([self.generated_label_name, self.drop_cols], axis=1).values
        classifier = RandomForestClassifier()
        classifier.fit(X,y)
        self.save_model(classifier, model_name='rand_forest')


    def generate_dict_till_date(self, date):
        """ Creates dataframes containing only data from before the specified date and puts them in the
        self.current_dynamic_data_dict so it can be used in temporal feature generation.
        
        Parameters
        ----------
        date : date
            The last data contained in the dataframe
        """
        for df_name, df_arg in self.dynamic_data_dict.items():
            df_all = df_arg["df"]
            date_col = df_arg["date_col"]
            df_till_date = df_all[(df_all[date_col] <= date)]
            df_arg["df"] = df_till_date
            self.current_dynamic_data_dict[df_name] = df_arg

    def construct_labels(self, date):
        """ Constructs labels for training and testing
        
        Parameters
        ----------
        date : date
            final date in the dataframe, date on which you want to determine whether people are going to churn
        
        Returns
        -------
        pandas.DataFrame
            dataframe containing a label for each id
        """
        label_generator = LabelGenerator(self.current_dynamic_data_dict, date)
        self.label_df = label_generator.generate_labels()

    def generate_forecast(self, date_range, eval_in_line=False):
        """ Generates forecasts for different points in time.
        
        Parameters
        ----------
        date_range : list
            a list containing dates that specify a point in time from where the prediction is made
        """
        self.merge_and_set_static_data()
        forecast_dict = {}
        for index, date in date_range.iteritems():
            print(20*'-' + "Start new forecast" + 20*'-')
            self.generate_dict_till_date(date)
            t_start = dt.datetime.now()
            self.merge_and_set_static_data()
            t_end = dt.datetime.now()
            print("Merging static data took: %s" %str(t_end-t_start), flush=True)
            t_start = dt.datetime.now()
            self.construct_labels(date)
            t_end = dt.datetime.now()
            print("Constructing labels took: %s" %str(t_end-t_start), flush=True)
            t_start = dt.datetime.now()
            self.generate_model_data()
            t_end = dt.datetime.now()
            print("Generating model data took: %s" %str(t_end-t_start), flush=True)
            t_start = dt.datetime.now()
            self.missing_data_treatment()
            t_end = dt.datetime.now()
            print("Missing label treatment took: %s" %str(t_end-t_start), flush=True)
            t_start = dt.datetime.now()
            forecast = self.make_forecast(eval_in_line)
            print("Making forcast took: %s" %str(t_end-t_start), flush=True)
            t_start = dt.datetime.now()
            forecast_dict[date] = forecast
        return forecast_dict

    def load_model(self):
        """ Loads the most recent model from the models folder
        
        Returns
        ----------
        pickle.model
            machine learning model
        """
        # use self.model_date
        list_of_files = glob.glob('../models/*.sav')
        # * means all if need specific format then *.csv
        model_location = max(list_of_files, key=os.path.getctime)
        model = pkl.load(open(model_location, 'rb'))
        return model

    def make_forecast(self, eval_in_line):
        """ Splits self.model_data in an X and a y and uses the X to make predictions with a previousely generated model
        and y to verify these predictions.
        
        Returns
        ----------
        y_pred
            the prediction based on X either in labels or in probabilities on label 1
        """
        model = self.load_model()
        y = self.model_data[self.generated_label_name].values
        used_df = self.model_data.drop([self.generated_label_name, self.drop_cols], axis=1)
        X = used_df.values
        if eval_in_line:
             y_pred = model.predict(X)
             accuracy = accuracy_score(y,y_pred)
             print('accuracy = %s' %str(accuracy))
        else:
             y_pred = model.predict_proba(X)
        return y_pred
