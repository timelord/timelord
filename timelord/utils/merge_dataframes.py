import time
from collections import defaultdict

class MergeDataframes(object):
    """A class to ease the joining of multiple dataframes for further analysis.
    This class handles:

    - joining multiple dataframes;
    - tracking duplicate column names;
    - verifying if the duplicate column names can be removed if their contents are identical;
    - cleans unwanted column names;
    - allows pre-join callbacks;
    - handles normalization;

    This class is build on top of the pandas `.merge()` method and handles everything. 
    There is no need to use this class for easy joins but rather for long piped joins 
    of which a lot of cleaning needs to be done. Joining is done based on a dictionary with the key being
    the dataframe name and the value being the dataframe. In this dictionary all the necessary 
    dataframes are present, both for joining and for normalization.
    
    Everything is joined against the base dataframe and only one dataframe is returned.

    Parameters
    ----------
    logger : Logging.logger, optional
        logger object from the Logging python module. Can be provided if logging is desired. (default = None) 
           
    undo_renaming : boolean
        Remove the prepended column names that have been set before joining. If false the column names will still have their table_name prepended after joining. (default = True)
    
    clear_duplicate_columns : boolean
        When two column names of different dataframes are identical they can be checked on their contents. If `True` one of the columns will be removed, if `False` both column names will still be present with their table name prepended.(default = True)
    
    base_df_name : string
        The name of the dataframe onto which everything is joined. (default = True)
    
    keep_columns : dictionary    
        WARNING: If the dataframe is not noted and no column names are specified everything will be removed. So when an empty dictionary is provided everything will be removed after joining. If everything needs to be kept it can be fixed by providing the `keep_columns` argument with the following so everything is kept: `keep_columns={df_name: df_dict[df_name].columns for df_name in df_dict.keys()}` (default = {})
        
    pre_join_callback_map : dictionary
        A dictionary with a function mapped to the name of the dataframe. NOTE: Takes as input a dataframe and must have as output another dataframe. (default = {})
    
    df_dict : dictionary
        The dictionary with all the dataframes for joining and normaliation. The key is the name of the dataframe and the value is the dataframe itself. (default = {})

    df_relations : list of tuples
        A list with nested tuples like: `[((LEFT_DATAFRAME_NAME, LEFT_COLUMN_NAME), (RIGHT_DATAFRAME_NAME, RIGHT_COLUMN_NAME), HOW), (...)]` (default = [])
         
        or as an example:
        `[(('df1', 'column_1'), ('df2', 'column_X'), 'left),  (...)]`
        
        The order and logic of joining is set in this list and is essential for joining. View the tests for
        more examples. the HOW accepts the same methods as defined in pandas for `.join()` and `.merge()`.
    
    id_name_map_df : dictionary
        A dictionary with the to be mapped column name as key and a tuple as value. In the tuple the name of the dataframe that can be used for normalization is on the left and on the right a list with the KEY VALUE name pair is set. (default = {})
    
        {TO_BE_NORMALIZED_COLUMN_NAME: (NAME_NORMALIZE_DATAFRAME, [NORMALIZE_KEY, NORMALIZE_VALUE]), ... }
        
        Is called _after_ joining all the dataframes to normalize the contents of the dataframe
        with a mapping. Another example:
        
        {'Product_Module_Group_ID': ('Product_Module_Group', ['ID', 'Name'] ), ... }
    
    _base_df : None (private)
        The actual dataframe onto which everything is joined. This is the final dataframe.

    Examples
    --------  
        By setting the arguments that are described above and call
        Advised is using a small subset of dataframes to catch any index / keyerrors without waiting too long.
        
        The only line of code actually used is the following below:
        
        >>> MergeDataframes(**config).merge_relations()
    """

    
    #Remove explicit reference in the keep_columns dict (if a table is used in the relation but None of the columns are kept it still has to occur in the keep_columns dict)
    def __init__(self, **kwargs):
        self.logger = None
        self.undo_renaming=True #Each column name is prepended with the table name to avoid column name collisions, when True this will be done for each name except for the one in collision.
        self.clear_duplicate_columns=True #Each column that has a collision will be checked if the contents are identical, if so, only one of the column is kept and the original name is kept.
        self.base_df_name = None
        self.keep_columns = {} #Is called AT THE END, after everything is joined.
        self.pre_join_callback_map = {} #Is called right after opening
        self.df_dict = {} #Actually contains the dataframes with their name
        self.df_relations = {} #The table relation mapping dict
        self.id_name_map_df = {}
        self._base_df = None
        self._joined_tables = []
        self._rename_dict_map = {} #Used between merging to track the renaming of the dataframes
        if kwargs is not None:
            for attr, value in kwargs.items():
                setattr(self, attr, value)        
        if self.logger is not None:
            self.logger.info("init MergeDataFrames with args: {} and kwargs: {}".format(args, kwargs))
            
    def _open_df(self, df_name, side):
        if side == 'left' and self._base_df is not None:
            #This is done so the left_df is still refered to as left_df in the merge_relations method.
            return self._base_df
           
        df = self.df_dict[df_name]
        if df_name in self.pre_join_callback_map.keys():
            df = self.pre_join_callback_map[df_name](df)
        self._rename_dict_map[df_name] =  self._get_rename_dict(df_name, df)
        df = df.rename(columns = self._rename_dict_map[df_name])
        return df
    
    def _init_base_df(self): #Add exception catching?
        self._base_df = self._open_df(self.base_df_name, 'left')
        if self.logger:
            self.logger.info('base_df set: {} with shape: {}'.format(self.base_df_name, self._base_df.shape))
            
    def _get_rename_dict(self, df_name, df):
        return {col: f'{df_name}_{col}' for col in df.columns}

    def _set_shared_map(self):
        self._do_not_rename_cols = defaultdict(list)
        for t1 in self._rename_dict_map.keys(): #Iterate over the tables
            for t2 in self._rename_dict_map.keys():
                if t1 != t2 and t1 in self.keep_columns.keys() and t2 in self.keep_columns.keys():
                    t1_columns = self._rename_dict_map[t1].keys()
                    t2_columns = self._rename_dict_map[t2].keys()
                    shared_columns = list(set.intersection(set(t1_columns), 
                                                           set(t2_columns),
                                                           set(self.keep_columns[t1]),
                                                           set(self.keep_columns[t2])))
                    if len(shared_columns) > 0:
                        for shared_col in shared_columns:
                            self._do_not_rename_cols[shared_col].append(self._rename_dict_map[t1][shared_col])
                            self._do_not_rename_cols[shared_col].append(self._rename_dict_map[t2][shared_col])
                        self._do_not_rename_cols[shared_col] = list(set(self._do_not_rename_cols[shared_col]))
                                          
                                
    def _set_complete_reverse_rename_map(self):
        self._complete_reverse_rename_map = {}
        for table in self._rename_dict_map:
            for original, new in self._rename_dict_map[table].items():
                self._complete_reverse_rename_map[new] = original

    def _set_keep_column_list(self):
        self._keep_column_list = []
        keep_columns_tables = list(set.intersection(set(self._rename_dict_map.keys()),
                                                    set(self.keep_columns.keys())))
        for table in keep_columns_tables:
            table_map = self._rename_dict_map[table]
            keep_columns = self.keep_columns[table]
            for col in keep_columns:
                self._keep_column_list.append(table_map[col])

    def _set_keep_duplicate_name_columns(self):
        #ALl columns that have an identical name are checked on their contents if they are equivalent.
        #Identical e.g. table X has column Customer and Table Y has Customer as well.
        #If they are named equivalent and do not share the same content they are kept 
        #If they are named equivalent and do share only one of the originals is chosen to be kept.
        self._keep_duplicate_name_columns = []
        for org_key in self._do_not_rename_cols:
            if len(self._do_not_rename_cols[org_key]) > 0:
                slice_columns = list(set(self._do_not_rename_cols[org_key]))
                df_slice = self._base_df[slice_columns] #Get only the dataframe that have shared keys
                identical = {}
                for c1 in df_slice.columns:
                    for c2 in df_slice.columns:
                        if c1 != c2 and (c2, c1) not in identical.keys():
                            identical[(c1, c2)] = all(df_slice[c1] == df_slice[c2]) #Check if all values are identical
                #If both columns are identical than the boolean is set to true, if false they are kept in keep_duplicates
                keep_duplicates = list(filter(lambda x: x[0] if x[1] != True else False, identical.items()))
                if len(keep_duplicates) > 0:
                    for ((keep_c1, keep_c2), _bool) in keep_duplicates: #These columns will not be renamed
                        self._keep_duplicate_name_columns.append(keep_c1)
                        self._keep_duplicate_name_columns.append(keep_c2)
                else:
                    keep_column = list(map(sorted, list(identical.keys())))[0][0] #These columns will be renamed #Why need to be sorted?
                    self._rename_cols.append(keep_column)
                  
    def _set_rename_cols(self):
        self._do_not_rename_cols_unfolded = [col_list for col in self._do_not_rename_cols.values() for col_list in col]
        self._rename_cols = [col for col in self._keep_column_list if col not in self._do_not_rename_cols_unfolded]
        self._set_keep_duplicate_name_columns()

    def _set_rename_col_map(self):
        #Sets the final rename mapping, including resolving the double name columns based on 
        #their contents. If similar named columns still exist they will still be added to the renaming
        #dict, but will be renamed to their mapped name. 
        #E.g. TABLENAME_columnname : TABLENAME_columnname instead of TABLENAME_columname: columname
        self._set_shared_map()
        self._set_complete_reverse_rename_map()
        self._set_rename_cols()
        self._rename_col_map = {col: self._complete_reverse_rename_map[col] for col in self._rename_cols}
        for col in self._keep_duplicate_name_columns:
            self._rename_col_map[col] = col
        
    def _undo_renaming(self):
        self._set_keep_column_list()
        self._set_rename_col_map()
        self._base_df = self._base_df[list(self._rename_col_map.keys())]
        self._base_df = self._base_df.rename(columns=self._rename_col_map)
        
    def _apply_id_name_mapper(self):
        for column, mapper in self.id_name_map_df.items():
            map_df_name, map_columns = mapper
            map_vals = self.df_dict[map_df_name][map_columns].drop_duplicates().values
            self._base_df[column] = self._base_df[column].map(dict(map_vals))    

    def _quick_merge(self, left_df, left_df_column,
                    right_df, right_df_column,
                    how):
        left_df.index = left_df[left_df_column] #Dont use set_index so we don't lose the original
        right_df.index = right_df[right_df_column]
        new_df = left_df.join(right_df, how = how) #join is ~10x as fast as .merge
        return new_df
        
    def _merge_dfs(self, left_df_name, left_df, left_df_column, 
                     right_df_name, right_df, right_df_column, 
                     how = 'left'):
        new_left_column = self._rename_dict_map[left_df_name][left_df_column]
        new_right_column = self._rename_dict_map[right_df_name][right_df_column]
         
        new_df = self._quick_merge(left_df, new_left_column,
                                   right_df, new_right_column,
                                  how)
        return new_df

    def merge_relations(self):
        self._init_base_df()
        for ((left_df_name, left_df_column), (right_df_name, right_df_column), how) in self.df_relations:
            if self.logger:
                self.logger.info((self._base_df.shape, left_df_name, left_df_column, right_df_name, right_df_column, how))
            try:
                #Open the dataframes for joining
                left_df = self._open_df(left_df_name, 'left')
                right_df = self._open_df(right_df_name, 'right')
                #Build rename dict map for tracking
                self._base_df = self._merge_dfs(left_df_name, left_df, left_df_column,
                                                  right_df_name, right_df, right_df_column,
                                                  how)
            except Exception as e:
                if self.logger is not None:
                    self.logger.exception("Error raised while trying to join: {}".format((self._base_df.shape, left_df_name, left_df_column, right_df_name, right_df_column, how)))
                raise e
            
        try:
            if self.undo_renaming:
                self._undo_renaming()
        except Exception as e:
            if self.logger is not None:
                self.logger.exception("Error raised while trying to undo renaming: {}".format((self._base_df.shape, left_df_name, left_df_column, right_df_name, right_df_column, how)))
            raise e
        self._base_df = self._base_df.drop_duplicates().reset_index(drop=True) 
        self._apply_id_name_mapper()
     