import pandas as pd
import numpy as np

class LabelGenerator(object):
    def __init__(self, current_df_dict, date):
        self.current_df_dict = current_df_dict
        self.date = date
        self.relevant_df = None

    def generate_labels(self):
        """ Generates churn labels for the unlabeled data provided in current_df_dict
        
        Returns
        -------
        pandas.DataFrame
            A dataframe containing the ID and the (currently binary) labels corresponding to the ID's.
        """
        self._get_relevant_df()
        label_df = self._generate_label_per_id()
        return label_df

    def _get_relevant_df(self):
        """ Get the dataframe that contains the data necessary for determining the existence of churn and mold it into
        the right format. The dataframe is saved in self.relevant_df
        """
        self.relevant_df = self.current_df_dict['trans_df']['df']
        self.relevant_df.index = range(self.relevant_df.shape[0])
        self.relevant_df['transaction_date'] = pd.to_datetime(self.relevant_df['transaction_date'], format="%Y%m%d")


    def _generate_label_per_id(self):
        """When the last transaction is more than 3 months ago, change the 0 in the column churn_label to a 1
        
        Parameters
        ----------
        label_df : pandas.DataFrame
            Dataframe containing the columns churn_label and msno, where the churn_label is always 0
        
        Returns
        -------
        pandas.DataFrame
            Dataframe where the churn_label contains 1 if the last transaction of that customer was more than 3 months before the date
        """
        unique_msno = self.relevant_df['msno'].unique()
        reference_date = self.date - pd.DateOffset(months=3)
        df_non_churned_slice = self.relevant_df.loc[self.relevant_df['transaction_date'] > reference_date]
        unique_non_churned_msno = df_non_churned_slice['msno'].unique()
        label_non_churned_df = pd.DataFrame({'msno': unique_non_churned_msno,
                                         'churn_label': np.ones(unique_non_churned_msno.size)})
        unique_churned_msno = [x for x in unique_msno if x not in unique_non_churned_msno]
        label_churned_df = pd.DataFrame({'msno': unique_churned_msno,
                                             'churn_label': np.zeros(len(unique_churned_msno))})
        label_df = pd.concat([label_churned_df, label_non_churned_df])
        return label_df