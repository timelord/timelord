import numpy as np
from sklearn.preprocessing import Imputer
class PrepareData(object):

    #TODO: Make the Imputer class into an argument
    def __init__(self): 
        self.imp = Imputer(missing_values=np.float('nan'),
                           strategy="mean",
                           axis=1) #Impute alongside the Rows, not columns
    
    #Generalize and move to TempFeatGen
    def get_temporal_features_dict(self, df):
        col_group = defaultdict(list)
        remaining = []
        for col in df.columns:
            if '_' in col:
                nw_col = '_'.join(col[::-1].split('_')[1:])[::-1]
                col_group[nw_col].append(col)
            else:
                remaining.append(col)
        return col_group, remaining

    
    def prep(self, df):
        df = df.drop('Intern', axis =1)
        col_group, remaining = self.get_temporal_features_dict(df)
        for column_set in col_group.values():
            complete_null_rows_loc = df[column_set].isnull().all(axis =1)
            df.loc[complete_null_rows_loc, column_set] = -1 #Rows that are completely empty are set to -1
            df[column_set] = self.imp.fit_transform(df[column_set]) #The remainder gets imputed
        df = df.fillna(-1) #Values that haven't been imputed / missed are filled with -1 anyway.
        return df
    
    
def _get_percentage_missing(series):
    """ Calculates percentage of NaN values in DataFrame
    
    Parameters
    ----------
    series: pandas.DataFrame
        Pandas DataFrame object
    
    Returns
    -------
    float

    """
    num = series.isnull().sum()
    den = len(series)
    return round(num/den, 3)


def _treat_rows_with_missing_data(df, row_percentage_dict):
    """ Deletes rows that consist for more than 80% of missing values
    
    Parameters
    ----------
    df : pandas.DataFrame
        dataframe that possibly contains missing values
    
    Returns
    -------
    pandas.DataFrame
        dataframe without rows that consist for more than 80% of missing values
    """
    df_nans_rows = df[df.isnull().all(axis=1)]
    #Could be made more efficient, since it now loops over all rows when there are columns that contain
    #only nans
    for row_index in df_nans_rows.index.values:
        percentage_row = _get_percentage_missing(df_nans_rows.loc[row_index, :])
        row_percentage_dict[str(row_index)] = percentage_row
        print(row_percentage_dict)
        if percentage_row > 0.8:
            df = df.drop(row_index, axis=0)
    return df, row_percentage_dict


def _treat_columns_with_missing_data(df,  column_percentage_dict):
    """ Deletes columns that consist for more than 50% of missing values, replaces the missing values
    with the average in the other columns
    
    Parameters
    ----------
    df : dataframe that possibly contains missing values
    
    Returns
    -------
    pandas.DataFrame
        dataframe without missing values
    """
    df_nans_columns = df[df.columns[df.isnull().any()].tolist()]
    for col_name in df_nans_columns.columns.values:
        percentage_col = _get_percentage_missing(df_nans_columns.loc[:, col_name])
        column_percentage_dict[col_name] = percentage_col
        if percentage_col > 0.5:
            if col_name == 'churn_label':
                df[col_name] = df[col_name].fillna(0) 
            else:
                df = df.drop(col_name, axis=1)
        else:
            df[col_name] = df.loc[:, col_name].fillna(df.loc[:, col_name].mean())
    return df, column_percentage_dict


def treat_missing_data(df):
    """ Creates a dataframe where all missing values are either replaced or removed
    
    Parameters
    ----------
    df : pandas.DataFrame
        dataframe that possibly contains missing values
    
    Returns
    -------
    pandas.DataFrame
        dataframe without missing values
    """
    row_percentage_dict = {}
    column_percentage_dict = {}
    df, row_percentage_dict = _treat_rows_with_missing_data(df, row_percentage_dict)
    df, column_percentage_dict = _treat_columns_with_missing_data(df, column_percentage_dict)
    return df, row_percentage_dict, column_percentage_dict
