import pandas as pd
import numpy as np
import time
import redis
import math
from multiprocessing import Pool

def pack(x):
    return x.to_msgpack(compress = 'zlib')

def unpack(x):
    return pd.read_msgpack(x)

#author Casper Lubbers
class DataFrameRedis(object):
    """
    Packs and unpacks a complete pandas DataFrame into a message pack #https://msgpack.org/
    to enable writing the data swiftly away to store it, though volatile, in Redis.
    Usage is only by the .get_df and .set_df methods. 
    If a DataFrame is to large to compress and write it in a single chunk 
    it gets automatically split up in several chunks. Chunk_size is given by the optional argument of the .set_df method.
    Write chunk_size defaults to 10**6 bits (based on the DataFrame.size)

    One can also retrieve with .get_df several chunks instead of the default all chunks.
    It also flushes the key name by default to prevent duplicating data.

    Parameters
    ----------
    _flush_df : boolean
        default = True
        Decides if the .set_df method flushes the key when writing to the key in Redis.
        
    use_multiprocessing : boolean
        default = False
        Decides if the .set_df and .get_df methods unpack and pack the chunks utilizing the multiprocessinig library of Python
        NOTE: Optimization is highly volatile varying to cpu_cores-1 X the speed-up to 10 times as slow (extremely long running threads).
        
    redisConnection : RedisConnectionSession
        default = None
        If another Redis Session is available or preferable this can be used. Else it initializes a new Redis session.
    """
        
    def __init__(self, _flush_df = True, use_multiprocessing = False, redisConnection = None, logging = False, redis_kwargs = None):
        if redis_kwargs is None:
            self.redis_kwargs = {'host':'localhost',
                                 'port':6379, 
                                 'db':0}
        else:
            self.redis_kwargs = redis_kwargs
            
        self._flush_df = _flush_df #Move to their respective (get set) methods?
        self._use_multiprocessing = use_multiprocessing #Move to their respective (get set) methods?

        self.redisCon = redis.StrictRedis(**self.redis_kwargs) if redisConnection is None else redisConnection
        self.logging = logging
        
    def __split_df(self, df, chunk_size):
        """
        Breaks the dataframe in chunks dependent on the chunk_size.
        
        Parameters
        ----------      
        df : DataFrame object
        The DataFrame to be split
        
        chunk_size : integer / float
        The amount of bits that the DataFrame can be without being split into the amount of chunks.  
        
        Returns
        -------
        array 
            array of DataFrames
        """
        
        ts = time.time()
        chunk_count = math.ceil(df.size / chunk_size)
        dfs = np.array_split(df, chunk_count) #Lets the RAM use grow (as peak) like this
        if self.logging:
            print('split dfs: shape: {}, chunk_count: {}, time: {}'.format(df.shape, chunk_count, time.time() - ts))
        return dfs

    def __pack_dfs(self, dfs, method = 'pack'):
        """
        Packs the DataFrame chunks into several msgpacks.
        
        Parameters
        ----------      
        dfs : list
            A list of DataFrame objects.
        
            method : string
            default = 'pack'
            options = 'pack'/ 'unpack'
            The function that is used to pack the DataFrame chunk.
        
        Returns
        -------
        list 
            list of msgpacks OR list of DataFrame chunks
                
        """
        ts = time.time()
        if self._use_multiprocessing:
            with Pool() as p:
                dfs = p.map(eval(method), dfs) #Maybe use apply_async? https://stackoverflow.com/questions/8533318/multiprocessing-pool-when-to-use-apply-apply-async-or-map
        else:
            dfs = list(map(eval(method), dfs))
        if self.logging:
            print('{} dfs: {}'.format(method, time.time() - ts))
        return dfs

    def __get_df_chunks(self, df, chunk_size):
        """
        Calculates the amount of chunks the DataFrame is to be broken up to.
        
        Parameters
        ----------      
        df : DataFrame object
            The DataFrame object that is used to calculate the amount of chunks.
        
        chunk_size : integer / float
            The amount of bits that the DataFrame can be without being split into the amount of chunks.  
        
        Returns
        -------
        list 
            list of msgpacks
        """
        if df.size < chunk_size:
            dfs = [df]
        else:
            dfs = self.__split_df(df, chunk_size)
        dfs = self.__pack_dfs(dfs, method = 'pack')
        return dfs

    def __clear_redis_list(self, name):
        """
        Clears the key in the Redis session to prevent duplicating data.
        
        Parameters
        ----------      
        name : string
            The key that is used for .set_df and .get_df    
                
        Returns
        -------
        list 
            list of msgpacks
        """
        llist = self.redisCon.llen(name)
        if self.logging:
            print("Deleting lrange of length: {}".format(llist))
        if llist > 0:
            for i in range(llist):
                self.redisCon.rpop(name)
        return True

    def __fetch_df(self, name, chunk_count):
        """
        Uses the lrange method of the redis connection to pull the chunks into Python.
        
        Parameters
        ----------      
        name : string
            The key that is used for .set_df and .get_df   
        
        chunk_count : integer
            The amount of chunks that are to be pulled into Python.
                        
        Returns
        ------- 
        list 
            list of DataFrames
        """
        s = time.time()
        if chunk_count == 'max' or chunk_count >= self.redisCon.llen(name):
            chunk_count = self.redisCon.llen(name)        
        df_list = self.redisCon.lrange(name, 0, chunk_count)
        if self.logging:
            print("DataframeRedis.fetch_df", name, time.time() - s)
        return df_list

    def __push_dfs(self, name, cdfs):
        """
        Uses the lrange method of the redis connection to pull the chunks into Python.
        
        Parameters
        ----------      
        name : string
            The key that is used for .set_df and .get_df   
        
        cdfs : list
            The list of DataFrame objects that are to be written to Redis.
        """
        s = time.time()
        for i, cdf in enumerate(cdfs):
            self.redisCon.lpush(name, cdf)
            if i % 1000 == 0 and self.logging:
                print( "DataframeRedis.write_df", i+1, name, len(cdfs), time.time() - s)

    def set_df(self, name, df, chunk_size = 10**6):
        """
        Writes the DataFrame object to Redis as a msgpack object. If the DataFrame is larger than the chunk_size
        it gets split up into several chunks so that Redis doesn't break. 
        
        Parameters
        ----------      
        name : string
            The key that is used for .set_df and .get_df   
        
        df : pandas.DataFrame
            The DataFrame object that is to be written to Redis as a msgpack
        
        chunk_size : integer / float
            default = 10**6
            The maximum size the DataFrame can be before being broken into chunks.
            Max chunk_size is advised to keep below 1 MB (current). Can handle up to ~1GB so you can try to tweak it for better performance. Biggest speed loss is at the splitter & compressing. Redis is very fast.
        """
        s = time.time()
        self.cdfs = self.__get_df_chunks(df, chunk_size)
        if self._flush_df:
            self.__clear_redis_list(name)
        self.__push_dfs(name, self.cdfs)
        if self.logging:
            print ("Done writing dfs", time.time() - s)
        return True
    
    def get_df(self, name, chunk_count = 'max', sort_index = False):
        """
        Retrieves the msgpacks from the Redis database, unpacks them and concats the DataFrame chunks into one large Dataframe.
        
        Parameters
        ----------      
        name : string
            The key that is used for .set_df and .get_df   
        
        chunk_count : string / integer / float
            default = 'max'
            The amount of chunks that are to be retrieved from the Redis database.
        
        sort_index : Boolean
            default = False
            Sorts the retrieved DataFrame in order based on the index. Often happens that order isn't retained.
                                
        Returns
        -------
            pandas.DataFrame
        """
        df_list = self.__fetch_df(name, chunk_count)
        pulled_dfs = self.__pack_dfs(df_list, method = 'unpack')
        if len(pulled_dfs) > 0:
            pulled_dfs = pd.concat(pulled_dfs)
        if sort_index:
            pulled_dfs = pulled_dfs.sort_index()
        return pulled_dfs
    
    #util
    def get_df_chunk_count(self, name):
        return self.redisCon.llen(name)
    #util
    def delete_df(self, name):
        return self.redisCon.delete(name)
    #util
    def get_df_names(self):
        return list(map(lambda x: x.decode('utf-8'), self.redisCon.keys()))
        
