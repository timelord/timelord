import pandas as pd
import numpy as np
import math
import datetime
from statsmodels.tsa.arima_model import ARIMA

class Benchmark(object):
    """
    Compute several forecasts and calculates multiple  error metrics.

    The Benchmark class creates a benchmark instance with some standard forecasting methods and error metrics.
    The supported forecasting methods are: 'naive','seasonal_naive','arima', 'drift' and 'average'. The supported metrics for measuring forecast error are: 'mae', 'rmse', 'mape' and 'smape'. By default all forecasting methods and all error metrics are calculated.
    
    The time series gets split into a train and test set based on forecast_length, i.e. the last forecast_length observations of the  timeseries will be in the test set while the remainder will be placed in the training set. The forecast values can be accessed via the  forecasts_df attribute, while the error metrics for these forecasts can be accessed via the results attribute. The attributes can also be accessed via the get_results() and get_forecasts() methods.

    Parameters
    ----------
    naive : bool
	    Calculate the naive forecast. For details about the forecasting method,  [1]_.
    seasonal_naive : bool
	    Calculate the seasonal naive forecast. For details about the forecasting method,  [1]_.
    arima : bool
	    Calculate an ARIMA forecast. For details about the forecasting method, [1]_.
    drift : bool
	    Calculate a forecast with drift. For details about the forecasting method, [1]_.
    average : bool
	    Calculates a forecast based on the average value. For details about the forecasting method, [1]_.
    keep_forecasts : bool
        Store the forecast in an attribute.  
    rmse : bool
	    Calculate the root mean squared error. For details about the error metrics,  [1]_.
    mae : bool
	    Calculate the mean absolute error. For details about the error metrics,  [1]_.
    mape : bool
        Calculate the mean absolute percentage error. For details about the error metrics,  [1]_.
    smape : bool
	    Calculate the symmetric mean absolute percentage error. For details about the error metrics,  [1]_.

    Attributes
    ----------
    forecasts : dict
	    Dictionary with the values of which forecast methods should be used.
    mappings_fc : dict
	    Dictionary mapping mapping parameters to function names.
    metrics : dict
	    Dictionary storing with metrics should be calculated.
    mappings_metrics : dict
	    Dictionary mapping parameters to function names.
    keep_forecasts : bool
    results : dataframe
	    DataFrame that contains the error metrics for the different forecast methods. 
    forecast_df : dataframe
	    Stores the forecasted values.

    Methods
    -------
    benchmark(self, df, forecast_length, seasonal_period = 0, order = (1,0,1))
	Calculates forecasts and errors. See also this methods own documentation.

    Examples
    --------
    
    >>> # Create some sample data
    >>> np.random.seed(1)
    >>> x = np.linspace(0, 4 * np.pi, 400)
    >>> sinx = np.sin(x)
    >>> error = np.random.normal(0,0.2,400)
    >>> y = sinx + error
    >>> index = pd.date_range(start = "2016-01-01", periods = 400)
    >>> ts = pd.DataFrame(y, index = index, columns = ["Time Series"])

    >>> # Specify the forecast methods in the parameters.
    >>> bm = Benchmark(arima = False, keep_forecasts = True) # by default 
    >>> does an arima(1,0,1), but the data is not stationary.
    >>> #  bm.benchmark(ts, forecast_length = 200, seasonal_period = 200)
    >>> # You can also get the metrics via the individual attributes:
    >>> bm.seasonal_naive_rmse
    >>> 0.28287028389225283

    >>> # Or use get_results()
    >>> bm.get_results()
    >>> # and similar for the forecasts
    >>> bm.get_forecasts()

    >>> # Load some data from timelord.datasets
    >>> from timelord.datasets.load_data import load_grocery
    >>> df = load_grocery().long_multi_var_data
    >>> df = df[df.index < "2017-8-16"] # removes the trailing zeros
    >>> ts = pd.DataFrame(df['all_products_all_stores_unit_sales'])

    >>> # Create the benchmark
    >>> bench = Benchmark()
    >>> bench.benchmark(ts, forecast_length = 14, seasonal_period = 7)

    References
    ----------    
    .. [1] Hyndman, R.J. and Athanasopoulos, G. (2013) Forecasting: principles and practice. OTexts: Melbourne, Australia. http://otexts.org/fpp/.

    Notes
    -----
    It is possible to add new forecast methods or error metrics.
    This note contains an example of adding a new forecast method. Error metrics are similar.
    Make sure the to be added function has the following structure.

    >>>	def my_new_forecast(self, kwarg1, kwarg2, ..., **kwargs):
		    \"\"\" your docstring in double quotes  \"\"\" 
    >>>   	# interesting code
    >>>   	return dataframe #pandas df with a single column containing the forecast values and a DateTimeIndex*

    Note the **kwargs at the end. This is necessary only necessary for forecasts and not for error metrics.

    To add this function do the following:

    1. add the function to the class;
    2. update the arguments of __init__  to reflect that a function has been added and specify a default;
    3. update the dictionaries self.forecasts and self.mappings_fc.
    """

    def __init__(self, naive = True, seasonal_naive = True, arima = True, drift = True, average = True, keep_forecasts = False, rmse = True, smape = True, mape = True, mae = True):
        
        self.forecasts = {'naive':naive,
                          'seasonal_naive':seasonal_naive,
                          'arima':arima,
                          'drift':drift,
                          'average':average
                         }
        
        self.mappings_fc = {'naive': self.naive_fc,
                            'average':self.average_fc,
                            'drift':self.drift_fc,
                            'seasonal_naive':self.snaive_fc,
                            'arima':self.arima_fc
                        }
        
        self.metrics = {'rmse':rmse,
                        'mae':mae,
                        'mape':mape,
                        'smape':smape                       
                       }
        
        self.mappings_metrics = {'rmse':self.metric_rmse,
                                 'mae':self.metric_mae,
                                 'mape':self.metric_mape,
                                 'smape':self.metric_smape
                                }
        self.keep_forecasts = keep_forecasts
        self.results = None
        self.forecasts_df = None        

    def break_df(self, df, forecast_length):
        """Create a train and test set.
        
        Parameters
        ----------
        df : pandas.DataFrame
            a pandas dataframe with number of rows > forecast_length.
        
        Returns
        -------
        pandas.DataFrame:
            A pandas dataframe; a train set.
        pandas.DataFrame:
            A pandas dataframe; a test set.
        
        """
        y_train = df.iloc[:-forecast_length]
        y_test = df.iloc[-forecast_length:]
        return y_train, y_test      
        
    def naive_fc(self, timeseries, forecast_length = 14, **kwargs):
        """Creates a forecast using the naive forecasting method, i.e. the last seen value.

        Parameters
        ----------
        timeseries : pandas.DataFrame
            A pandas DataFrame with an index which can be converted to DateTime. The first column must contain the time series.
        forecast_length: int
        **kwargs: dict
            For compatability with benchmark() 

        Returns
        -------
        pandas.DataFrame
            pandas DataFrame with number of rows equal to forecast_length, a DateTime index and a single column named "forecast".
        """
        date_range = pd.date_range(start = timeseries.index[-1], periods = forecast_length + 1)
        date_range = date_range[1:]
        predictions = timeseries.iloc[-1,0]
        return pd.DataFrame([predictions] * forecast_length, index = date_range, columns = ["forecast"])
    
    def average_fc(self, timeseries, forecast_length = 14, **kwargs):
        """Creates a forecast using the average forecasting method, i.e. the average over all values.

        Parameters
        ----------
        timeseries: dataframe
            A pandas DataFrame with an index which can be converted to DateTime. The first column must contain the time series.
        forecast_length: int
        **kwargs: dict
            For compatability with benchmark() 

        Returns
        -------
        pandas.DataFrame
            pandas DataFrame with number of rows equal to forecast_length, a DateTime index and a single column named "forecast".
        """
        date_range = pd.date_range(start = timeseries.index[-1], periods = forecast_length + 1)
        date_range = date_range[1:]
        predictions = np.mean(timeseries.iloc[:,0])
        return pd.DataFrame([predictions]*forecast_length, index = date_range, columns = ["forecast"])
    
    def drift_fc(self, timeseries, forecast_length = 14, **kwargs):
        """Creates a forecast using the drift forecasting method, i.e. linear extrapolation using the first and last value.

        Parameters
        ----------
        timeseries: dataframe
            A pandas DataFrame with an index which can be converted to DateTime. The first column must contain the time series.
        forecast_length: int
        **kwargs: dict
            For compatability with benchmark() 

        Returns
        -------
        pandas.DataFrame
            pandas DataFrame with number of rows equal to forecast_length, a DateTime index and a single column named "forecast".
        """
        date_range = pd.date_range(start = timeseries.index[-1], periods = forecast_length + 1)
        date_range = date_range[1:]
        drift = (timeseries.iloc[:,0][-1] - timeseries.iloc[0,0])/len(timeseries.index)
        predictions = [timeseries.iloc[:,0][-1]+ i*drift for i in np.arange(1, forecast_length+1) ]
        return pd.DataFrame(predictions, index = date_range,columns = ["forecast"])
    
    def snaive_fc(self, timeseries, forecast_length = 14, seasonal_period = 7, **kwargs):
        """Creates a forecast using the seasonal naive forecasting method, i.e. the the value in the last observed season.

        Parameters
        ----------
        timeseries: dataframe
            A pandas DataFrame with an index which can be converted to DateTime. The first column must contain the time series.
        forecast_length: int
        **kwargs: dict
            For compatability with benchmark() 

        Returns
        -------
        pandas.DataFrame
            pandas DataFrame with number of rows equal to forecast_length, a DateTime index and a single column named "forecast".

        Examples
        --------
        >>> x = np.linspace(0, 4 * np.pi, 400)
        >>> sinx = np.sin(x)
        >>> index = pd.date_range(start = "2016-01-01", periods = 400)
        >>> ts = pd.DataFrame(sinx, index = index, columns = ["Time Series"])

        >>> snaive_fc(ts, forecast_length = 100, seasonal_period = 200)

        """
        date_range = pd.date_range(start = timeseries.index[-1], periods = forecast_length + 1)
        date_range = date_range[1:]
        k = math.ceil(forecast_length / seasonal_period)
        period = timeseries.iloc[-seasonal_period:,0].tolist()
        kperiods = period * k
        predictions = kperiods[0:forecast_length]
        return pd.DataFrame(predictions, index = date_range, columns = ["forecast"])
    
    def arima_fc(self, timeseries, forecast_length = 14, order = (1,0,1), **kwargs):
        """Creates a forecast using the ARIMA method.

        Parameters
        ----------
        timeseries : dataframe
            A pandas DataFrame with an index which can be converted to DateTime. The first column must contain the time series.
        forecast_length : int
        **kwargs : dict
            For compatability with benchmark().

        Returns
        -------
        pandas.DataFrame
            Dataframe with number of rows equal to forecast_length, a DateTime index and a single column named "forecast".
        """

        date_range = pd.date_range(start = timeseries.index[-1], periods = forecast_length + 1)
        date_range = date_range[1:]
        model_fit = ARIMA(timeseries, order=order).fit(disp=0)
        predictions = model_fit.predict(start = timeseries.index[-1] + datetime.timedelta(days=1), end = timeseries.index[-1] + datetime.timedelta(forecast_length+1))
        return pd.DataFrame(predictions, index = date_range, columns = ["forecast"])

    def metric_mae(self, y_true, y_pred):
        """Calculates the mean absolute error.

        Parameters
        ----------
        y_true : array
            Array-like structure which can be converted to a numpy array.
        y_pred : array
            Array-like structure which can be converted to a numpy array.

        Returns
        -------
        float
            Scalar
        """

        y_true, y_pred = np.array(y_true), np.array(y_pred)
        return np.mean(np.abs(y_true - y_pred))

    def metric_mape(self, y_true, y_pred):
        """Calculates the mean absolute percentage error.

        Parameters
        ----------
        y_true : array
            Array-like structure which can be converted to a numpy array.
        y_pred : array
            Array-like structure which can be converted to a numpy array.

        Returns
        -------
        float
            Scalar
        """
        y_true, y_pred = np.array(y_true), np.array(y_pred)
        return 100 * np.mean(np.abs((y_true - y_pred) / y_true))

    
    def metric_rmse(self, y_true, y_pred):
        """Calculates the root mean squared error.

        Parameters
        ----------
        y_true : array
            Array-like structure which can be converted to a numpy array.
        y_pred : array
            Array-like structure which can be converted to a numpy array.

        Returns
        -------
        float
            Scalar
        """
        y_true, y_pred = np.array(y_true), np.array(y_pred)
    
        return np.sqrt(np.mean(np.square(y_true - y_pred)))
    
    def metric_smape(self, y_true, y_pred):
        """Calculates the SMAPE.
        
        Parameters
        ----------
        y_true : array
            Array-like structure which can be converted to a numpy array.
        y_pred : array
            Array-like structure which can be converted to a numpy array.

        Returns
        -------
        float
            Scalar
        """
        y_true, y_pred = np.array(y_true), np.array(y_pred)
        return 100 * np.mean(np.abs(y_true - y_pred) / (np.abs(y_true) + np.abs(y_pred)))
    
    def benchmark(self, df, forecast_length, seasonal_period = 0, order = (1,0,1)):
        """Calculates the error metrics for different forecasting methods and sets
        these errors as attributes of the Benchmark() instance.
        
        Parameters
        ----------
        df : pandas.DataFrame
	        The timeseries that will be passed on to break_df(). Needs to an (n x 1)-dataframe with forecast_length < n and a DateTime index.
        forecast_length : int
	        The length of the forecast.
        seasonal_period : int
	        Necessary argument for seasonal naive forecast. Defaults to 0.
        order : tuple of length 3
	        Necessary argument for the arima forecasting method. Defaults to (1,0,1).
        
        Returns
        -------
        pandas.DataFrame
	        Returns a DataFrame with the forecast methods as the rows and the error metrics as the columns.
        
        Raises
        ------
        ValueError
            If an invalid `seasonal_period` is given.

        Examples
        --------
	    For detailed examples, see the docstring of Benchmark()
        """
        
        if seasonal_period < 1 and self.forecasts['seasonal_naive']:
            raise ValueError("Please enter valid seasonal period.")
        
        self.results = pd.DataFrame(columns = [key for key in self.metrics],
                                    index = [key for key in self.forecasts])
                
        y_train, y_test = self.break_df(df, forecast_length)
        
        if self.keep_forecasts:
            self.forecasts_df = y_test.copy() # Initialize a dataframe with the correct DateTime indices.
            
        for forecast, boolean in self.forecasts.items():
            if boolean:
                y_pred = self.mappings_fc[forecast](timeseries = y_train, 
                                                    forecast_length = forecast_length,
                                                    seasonal_period = seasonal_period, 
                                                    order = order
                                                   )
                if self.keep_forecasts:
                    self.forecasts_df.loc[:, forecast] = y_pred
                    
                
                for metric, boolean2 in self.metrics.items():
                    if boolean2:
                        attribute = forecast + '_' + metric
                        metric_value = self.mappings_metrics[metric](y_test, y_pred)
                        setattr(self, attribute, metric_value )
                        self.results.loc[forecast, metric] = metric_value
                        
        return self.results
    
    def get_results(self):
        """Simple method to get the dataframe with results without having to call benchmark() again.
        """
        return self.results
        
    def get_forecasts(self):
        """Simple method to get the dataframe with forecast results without having to call benchmark() again.
        """
        return self.forecasts_df

