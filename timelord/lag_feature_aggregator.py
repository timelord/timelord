from sklearn.base import TransformerMixin, BaseEstimator

import pandas as pd
import numpy as np
from collections import defaultdict


def agg_mp_func(args):
    group, ops, id_col = args
    group = group.groupby(id_col)
    return group.agg(ops)

def date_mp_func(args):
    name, id_df, length_period, id_col, date_column, max_date = args
    if max_date is None:
        last_date_period = id_df[date_column].max()
    else:
        last_date_period = max_date
    first_date_period = last_date_period - pd.Timedelta(days=length_period)
    id_df_period = id_df.loc[
                            (id_df[date_column] >= first_date_period) & 
                            (id_df[date_column] < last_date_period)]
    return id_df_period    
    
class LagFeatureAggregator(BaseEstimator, TransformerMixin):
    """
    Generates lag features of operations on specified ranges of days

    1. Select data from period of specified number of days before the last measurement
    2. Group on ID
    3. Perform specified operations per ID on columns for which an operation is defined in the operations dictionary
    4. Create suiting column names
    5. Concat the dataframe to the dataframe of the previous period lengths to generate the final dataframe
    """

    def __init__(self, date_column, lags, ops, id_col, max_date, njobs=1):
        """
        Parameters
        ----------

        date_column : string
            Column containing the date
        lags : list
            A list of all different periods of days of which you want to make a lag feature
        ops : dict
            A dictionary of the desired operations per column
        id_col : string
            The name of the column containing the ID's
        max_date : date
            The maximum date.
        njobs : int, optional
            Number of jobs (default: 1)
        """
        self.date_column = date_column
        self.lags = lags
        self.ops = ops
        self.id_col = id_col
        self.max_date = max_date
        self.njobs = njobs
        self.lag_feature_map = {}

    def fit(self, X, y=None):
        return self
    
    def transform(self, X):
        """
        Creates lag features per ID

        Parameters
        ----------
        X : pandas.DataFrame
            Dataframe containing all data, indexed on ID and timestamp
        
        Returns
        -------
        pandas.DataFrame
            A dataframe containing the results of the operations on the columns per ID
        """
        for i, length_period in enumerate(sorted(self.lags, reverse=True)):
            df_period = self.__to_period_df(X, length_period, self.id_col, self.date_column, self.max_date)
            if df_period.empty:
                # The mock_df serves to generate the column names of the columns that contain nans since
                # they are based on no values (an empty df_period)
                df_period = self.__create_mock_df(X, self.id_col)
                agg_result = self.__period_to_lag_df(df_period, self.ops, length_period, self.id_col)
                # empty df that contains the IDs as an index
                empty_ids_df = pd.DataFrame(index=X[self.id_col].unique())
                # agg_result contains nan values for the lag features that are based on no measurements
                agg_result = empty_ids_df.reindex_axis(empty_ids_df.columns.union(agg_result.columns), axis=1)
            else:
                # agg_result contains the values of the aggregations selected in operations
                agg_result = self.__period_to_lag_df(df_period, self.ops, length_period, self.id_col)
            if i == 0:
                all_aggregated = agg_result
            else:
                #Merge the new lag feature in the df with the other lag features
                all_aggregated = all_aggregated.join(agg_result, how='left')
        self.lag_feature_map = self.__get_lag_features_dict(all_aggregated)
        return all_aggregated

    #Generalize and move to TempFeatGen
    def __get_lag_features_dict(self, df):
        col_group = defaultdict(list)
        for col in df.columns:
            if '_' in col:
                nw_col = '_'.join(col[::-1].split('_')[1:])[::-1]
                col_group[nw_col].append(col)
                
        for col in col_group:
            col_group[col] = tuple(col_group[col]) #Make the list tuple so it can be used later on as key
        return col_group
    
    def __to_period_df(self, df, length_period, id_col, date_column, max_date):
        """
        Retrieves the dataframe that only contains values from the selected period
        
        Parameters
        ----------
        df : pandas.DataFrame
            The dataframe on which the period dataframe is based
        length_period : int
            The number of days of the period on which the new lag feature will be based
        id_col : string
            The name of the column containing the ID's
        date_column : string
            Column containing the date

        Returns
        -------
        pandas.DataFrame
            A dataframe containing the last length_period days of each ID
        """
        id_groups = df.groupby(id_col)        
        # per ID retrieve the data from the number of days specified in length_period before the last
        # measurement for that ID concatenate that to one dataframe for that certain period length
        args = [(name, id_df, length_period, id_col, date_column, max_date) for name, id_df in id_groups]
        dfs = list(map(date_mp_func, args))
        if len(dfs) > 0:
            df_period = pd.concat(dfs)
        else:
            df_period = pd.DataFrame()
        return df_period

    def __create_mock_df(self, df , id_col):
        """

        Parameters
        ----------
        df : pandas.DataFrame
            The original dataframe containing all IDs and all original columns
        id_col : string
            The name of the column containing the ID's

        Returns
        -------
        pandas.DataFrame
            A dataframe containing one row with a zero value for each column
        """
        zero_array = np.zeros(len(df.columns.values))
        df_period = pd.DataFrame(columns=df.columns.values)
        df_period.loc[0] = zero_array
        df_period[id_col] = df[id_col]
        return df_period

    def __period_to_lag_df(self, df_period, operations, length_period, id_col):
        """
        Performs the aggregation on the dataframe containing the data of all IDs of one period

        Parameters
        ----------
        df_period : pandas.DataFrame
            Dataframe containing the values that are measured in the specified period
        operations : dict
            A dictionary of the desired operations per column
        length_period : int
            Int specifying the length (in days) of the current period
        id_col : string
            The name of the column containing the ID's

        Returns
        -------
        pandas.DataFrame
            Dataframe in which the operations have been performed on the columns, containing columns whose name is: feature + operation + length of the period
        """
        self.user_groups = df_period.groupby(id_col)
        args = [(group, operations, id_col) for _, group in self.user_groups]
        agg_results = map(agg_mp_func, args)
        agg_result = pd.concat(agg_results)
                
        agg_result.columns = [colname[0] + '_' + colname[1] + '_' + str(length_period) for colname in
                              agg_result.columns.values]
        if agg_result.shape[0] == 1 and agg_result.isnull().values.all():
            agg_result.iloc[0, :] = np.nan
        return agg_result
    
    def get_feature_importance_df(self, feature_df, importance_dict):
        feature_df_list = []
        for col in feature_df.columns:
            if '_' in col:
                splitted = col.split('_')
                temporality = splitted[-1]
                ops = splitted[-2]
                feature = ''.join(splitted[:-2])
    
            else:
                ops = None
                temporality = None
                feature = col
            feature_df_list.append([col, 
                                   importance_dict[col],
                                   temporality,
                                   ops,
                                   feature])
    
    
        importance_df = pd.DataFrame(feature_df_list, 
                     columns = ['feature',
                               'importance',
                               'lag',
                               'operation',
                               'feature'])
        return importance_df    

# ops = {
#         'salary': 'mean',
#         'children': ['nunique', 'mean'],
#           }
#
#
# LFG = LagFeatureGenerator(date_column = 'dates',
#                             lags = [100, 10, 2],
#                             ops = ops,
#                             id_col = 'pet',
#                             max_date = None,
#                             njobs = 1
#                             )
#
#
# from sklearn.pipeline import Pipeline
#
# steps = [
#     ['lag_feat', LFG],
# ]
#
# pipe = Pipeline(steps = steps)
# end_df = pipe.fit_transform(data)
#
# LFG.get_params