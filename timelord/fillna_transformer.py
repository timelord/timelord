import numpy as np
from sklearn.base import TransformerMixin, BaseEstimator
from sklearn.exceptions import NotFittedError

class FillNaTransformer(TransformerMixin, BaseEstimator):
    name = "FillNaTransformer"

    def __init__(self, fillna_args = None):
        self.fillna_args = fillna_args

    def transform(self, X):
        """
        Transform the dataframe to something without NAs

        Parameters
        ----------
        X : pandas.DataFrame
            The dataframe to transform

        Returns
        -------
        pandas.DataFrame
            The transformed dataframe.

        """
        if self.fillna_args is not None:
            if isinstance(self.fillna_args, dict):
                X = X.fillna(**self.fillna_args)
            else:
                X = X.fillna(self.fillna_args)

        return X

    def fit(self, X, y=None):
        """
        Nothing to fit

        Parameters
        ----------
        X : pandas.DataFrame
            The dataframe to fit

        Returns
        -------
        pandas.DataFrame
            The fitted dataframe.

        """
        return self

    def inverse_transform(self, X):
        """
        Nothing to inverse

        Parameters
        ----------
        X : pandas.DataFrame
            The dataframe to inverse

        Returns
        -------
        pandas.DataFrame
            The inversed dataframe.
        
        """
        return X
        
        
class PandasMethodTransformer(TransformerMixin, BaseEstimator):
    name = "PandasMethodTransformer"


    def __init__(self, method = None, method_kwargs = None):
        self.method = method
        self.method_kwargs = method_kwargs

    def transform(self, X):
        """
        Transform the dataframe to something without NAs

        Parameters
        ----------
        X : pandas.DataFrame
            The dataframe to transform

        Returns
        -------
        pandas.DataFrame
            The transformed dataframe.

        """
        if hasattr(X, self.method): #method can be fillna, interpolate, etc. etc.
            X = getattr(X, self.method)(**self.method_kwargs)
        return X

    def fit(self, X, y=None):
        """
        Nothing to fit

        Parameters
        ----------
        X : pandas.DataFrame
            The dataframe to fit

        Returns
        -------
        pandas.DataFrame
            The fitted dataframe.

        """
        return self

    def inverse_transform(self, X):
        """
        Nothing to inverse

        Parameters
        ----------
        X : pandas.DataFrame
            The dataframe to inverse

        Returns
        -------
        pandas.DataFrame
            The inversed dataframe.
        
        """
        return X

