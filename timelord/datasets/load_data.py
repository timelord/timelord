from os.path import dirname
import pandas as pd

class load_amazon_reviews(object):
#https://www.kaggle.com/snap/amazon-fine-food-reviews

    
    def __init__(self):
        self.path = dirname(__file__)
        self.amazon_reviews = pd.read_csv(dirname(__file__) + '/data/amazon_reviews/amazon_reviews.csv').drop('Unnamed: 0', axis = 1)

    
class load_churn(object):
#data from https://www.kaggle.com/c/kkbox-churn-prediction-challenge

    
    def __init__(self):
        self.path = dirname(__file__)
        self.members = pd.read_csv(dirname(__file__) + '/data/churn_data/members.csv')
        self.train = pd.read_csv(dirname(__file__) + '/data/churn_data/train.csv')
        self.user_logs = pd.read_csv(dirname(__file__) + '/data/churn_data/user_logs.csv')
        self.trans = pd.read_csv(dirname(__file__) + '/data/churn_data/trans.csv')
    
class load_grocery(object):
#data from https://www.kaggle.com/c/favorita-grocery-sales-forecasting
#some data, (multi-var data) calculated by Casper

    
    def __init__(self):
        self.path = dirname(__file__)
        self.items = pd.read_csv(dirname(__file__) + '/data/grocery_sales/grocery_items.csv')
        self.stores = pd.read_csv(dirname(__file__) + '/data/grocery_sales/grocery_stores.csv')
        self.holidays = pd.read_csv(dirname(__file__) + '/data/grocery_sales/grocery_holidays.csv')
        self.store_sales = pd.read_csv(dirname(__file__) + '/data/grocery_sales/grocery_store_sales.csv')
        self.item_sales = pd.read_csv(dirname(__file__) + '/data/grocery_sales/grocery_item_sales.csv')
        self.long_multi_var_data = self.reload_df(pd.read_csv(dirname(__file__) + '/data/grocery_sales/grocery_long_multi_var_data.csv'))
        self.short_multi_var_data = self.reload_df(pd.read_csv(dirname(__file__) + '/data/grocery_sales/grocery_short_multi_var_data.csv'))
        
        
    def reload_df(self, df):
        df.index = pd.DatetimeIndex(df['join_column_name'])
        return df.drop('join_column_name', axis = 1)
    
    
class load_movie_reviews(object):
##data from https://www.kaggle.com/c/kkbox-churn-prediction-challenge

    
    def __init__(self):
        self.path = dirname(__file__)
        self.movie_data = pd.read_csv(dirname(__file__) + '/data/imdb_reviews/movie_data.csv')

    
