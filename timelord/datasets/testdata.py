import pandas as pd
import numpy as np

def muscle_data():
    start_date = pd.to_datetime("01-01-2018", format="%d-%m-%Y")
    end_date = pd.to_datetime("01-02-2018", format="%d-%m-%Y")
    date_range = pd.date_range(start_date, end_date)
    keep_ix = [0, 1, 2, 3, 4, 5, 8, 9, 10, 12, 13, 17, 18, 19, 20, 25, 26, 27, 28, 29]
    date_range = list(date_range[keep_ix])
    date = date_range + date_range
    user = list(np.ones(len(date_range))) + list(np.zeros(len(date_range)))
    eye_color = list((2*np.ones(len(date_range)))) + list(np.ones(len(date_range)))
    sex = list(np.zeros(len(date_range))) + list(np.ones(len(date_range)))
    mg1 = [0, -11, 4, 2, 7, 25, 16, 35, 46, 35, 24, 33, 52, 38, 50, 68, 83, 71, 66, 55]
    mg2 = [-12, -8, 10, 6, 3, -1, 7, 20, 18, 27, 16, 29, 14, 24, 37, 46, 36, 37, 33, 38]
    muscle_growth = mg1 + mg2
    d_df = pd.DataFrame({'date': pd.to_datetime(date), 'user': user, 'eyecolor': eye_color, 'sex': sex, 'muscle_growth': muscle_growth})
    d_df['user'] = d_df['user'].astype(int)
    d_df = d_df[['date', 'user', 'eyecolor', 'sex', 'muscle_growth']]
    return d_df

def muscle_data_dynamic():
    start_date = pd.to_datetime("01-01-2018", format="%d-%m-%Y")
    end_date = pd.to_datetime("01-02-2018", format="%d-%m-%Y")
    date_range = pd.date_range(start_date, end_date)
    keep_ix = [0, 1, 2, 3, 4, 5, 8, 9, 10, 12, 13, 17, 18, 19, 20, 25, 26, 27, 28, 29]
    date_range = list(date_range[keep_ix])
    date = date_range + date_range
    user = list(np.ones(len(date_range))) + list(np.zeros(len(date_range)))
    mg1 = [0, -11, 4, 2, 7, 25, 16, 35, 46, 35, 24, 33, 52, 38, 50, 68, 83, 71, 66, 55]
    mg2 = [-12, -8, 10, 6, 3, -1, 7, 20, 18, 27, 16, 29, 14, 24, 37, 46, 36, 37, 33, 38]
    muscle_growth = mg1 + mg2
    d_df = pd.DataFrame({'date': pd.to_datetime(date), 'user': user, 'muscle_growth': muscle_growth})
    d_df['user'] = d_df['user'].astype(int)
    d_df = d_df[['date', 'user', 'muscle_growth']]
    return d_df

def muscle_data_static():
    start_date = pd.to_datetime("01-01-2018", format="%d-%m-%Y")
    end_date = pd.to_datetime("01-02-2018", format="%d-%m-%Y")
    date_range = pd.date_range(start_date, end_date)
    keep_ix = [0, 1, 2, 3, 4, 5, 8, 9, 10, 12, 13, 17, 18, 19, 20, 25, 26, 27, 28, 29]
    date_range = list(date_range[keep_ix])
    date = date_range + date_range
    user = list(np.ones(len(date_range))) + list(np.zeros(len(date_range)))
    eye_color = list((2*np.ones(len(date_range)))) + list(np.ones(len(date_range)))
    sex = list(np.zeros(len(date_range))) + list(np.ones(len(date_range)))
    d_df = pd.DataFrame({'date': pd.to_datetime(date), 'user': user, 'eyecolor': eye_color, 'sex': sex})
    d_df['user'] = d_df['user'].astype(int)
    d_df = d_df[['date', 'user', 'eyecolor', 'sex']]
    return d_df