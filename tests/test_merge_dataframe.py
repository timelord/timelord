import time
import json
import pandas as pd
import numpy as np
from copy import deepcopy
from timelord.utils.merge_dataframes import MergeDataframes



np.random.seed(42)

#build test data
size = 10
df1 = pd.DataFrame(np.random.random((size, 3)), columns = ['A', 'B', 'C'])
df2 = pd.DataFrame(np.random.random((size, 3)), columns = ['B', 'C', 'D'])
df3 = pd.DataFrame(np.random.random((size, 3)), columns = ['H', 'I', 'J'])

df1['global_shared'] = np.arange(size)
df2['global_shared'] = np.arange(size)
df3['global_shared'] = np.arange(size)

df1['join'] = np.arange(size)
df2['join'] = np.arange(size)

df2['not_shared_join'] = np.arange(size)
df3['not_shared_join'] = np.arange(size)


df1['equal'] = np.arange(size)
df2['equal'] = np.arange(size)
df3['equal'] = np.arange(size)

df_dict = {}
df_dict['df1'] = df1.copy()
df_dict['df2'] = df2.copy()
df_dict['df3'] = df3.copy()

df_relations = [(('df1', 'join'), ('df2', 'join'), 'left')]
keep_columns = {'df1': ['A', 'B', 'equal'],
               'df2': ['B', 'D', 'equal']}

keep_columns2 = {'df1': ['A', 'B', 'equal'],
               'df2': ['B', 'D', 'equal', 'not_shared_join'],
               'df3': ['H', 'equal', 'not_shared_join']}

df_relations2 = [(('df1', 'join'), ('df2', 'join'), 'left'), #df1 <> df2 && df2 <> df3 (different key)
                 (('df2', 'not_shared_join'), ('df3', 'not_shared_join'), 'left')]

df_relations3 = [(('df1', 'global_shared'), ('df2', 'global_shared'), 'left'), #df1 <> df2 && df2 <> df3 (shared key)
                 (('df2', 'global_shared'), ('df3', 'global_shared'), 'left')]

df_relations4 = [(('df1', 'global_shared'), ('df2', 'global_shared'), 'left'), #Same base df (df1)
                 (('df1', 'global_shared'), ('df3', 'global_shared'), 'left')]



def add_100_callback(df):
    df['B'] += 100
    return df

pre_join_callback_map = {'df1': add_100_callback}


def test_join_dfs_callback():
    mdf = MergeDataframes(df_dict = deepcopy(df_dict),
                      base_df_name = 'df1',
                      df_relations = df_relations,
                      keep_columns = keep_columns,
                      pre_join_callback_map = pre_join_callback_map)

    mdf.merge_relations()
    final_df = mdf._base_df
    print(final_df.columns)

    new_cols = ['A', 'D', 'equal', 'df2_B', 'df1_B']
    assert final_df.shape == (size, len(new_cols))
    assert len(list(set.intersection(set(list(final_df.columns)),
                               set(new_cols)))) == len(new_cols)
    assert all(final_df['equal'] == np.arange(size))
    assert all(final_df['A'] == df1['A'])
    assert all(final_df['df1_B'] != df1['B'])
    assert all(final_df['df2_B'] == df2['B'])
    assert all(final_df['D'] == df2['D'])
        
def test_join_dfs_no_callback():
    mdf = MergeDataframes(df_dict = deepcopy(df_dict),
                      base_df_name = 'df1',
                      df_relations = df_relations2,
                      keep_columns = keep_columns)

    mdf.merge_relations()
    final_df = mdf._base_df
    print(final_df.columns)
    new_cols = ['A', 'D', 'equal', 'df2_B', 'df1_B']
    assert final_df.shape == (size, len(new_cols))
    assert len(list(set.intersection(set(list(final_df.columns)),
                               set(new_cols)))) == len(new_cols)
    assert all(final_df['equal'] == np.arange(size))
    assert all(final_df['A'] == df1['A'])
    assert all(final_df['df1_B'] == df1['B'])
    assert all(final_df['df2_B'] == df2['B'])
    assert all(final_df['D'] == df2['D'])
    
def test_join_dfs_multiple_joins_1():
    new_keep_columns = deepcopy(keep_columns2)
    new_keep_columns['df1'].append('join')
    
    mdf = MergeDataframes(df_dict = deepcopy(df_dict),
                      base_df_name = 'df1',
                      df_relations = df_relations2,
                      keep_columns = new_keep_columns)

    mdf.merge_relations()
    final_df = mdf._base_df
    
    print(final_df.columns, final_df.shape)
    new_cols = ['A', 'join', 'D', 'H', 'equal', 'not_shared_join', 'df2_B', 'df1_B']
    assert final_df.shape == (size, len(new_cols))
    assert len(list(set.intersection(set(list(final_df.columns)),
                                     set(new_cols)))) == len(new_cols)
    assert all(final_df['equal'] == np.arange(size))
    assert all(final_df['A'] == df1['A'])
    assert all(final_df['join'] == df1['join'])
    assert all(final_df['df1_B'] == df1['B'])
    assert all(final_df['df2_B'] == df2['B'])
    assert all(final_df['D'] == df2['D'])

def test_join_dfs_multiple_joins_2():
    new_keep_columns = deepcopy(keep_columns2)
    new_keep_columns['df1'].append('join')
    
    mdf = MergeDataframes(df_dict = deepcopy(df_dict),
                      base_df_name = 'df1',
                      df_relations = df_relations3,
                      keep_columns = new_keep_columns)

    mdf.merge_relations()
    final_df = mdf._base_df
    
    print(final_df.columns, final_df.shape)
    new_cols = ['A', 'join', 'D', 'H', 'equal', 'not_shared_join', 'df2_B', 'df1_B']
    assert final_df.shape == (size, len(new_cols))
    assert len(list(set.intersection(set(list(final_df.columns)),
                                     set(new_cols)))) == len(new_cols)
    assert all(final_df['equal'] == np.arange(size))
    assert all(final_df['A'] == df1['A'])
    assert all(final_df['join'] == df1['join'])
    assert all(final_df['df1_B'] == df1['B'])
    assert all(final_df['df2_B'] == df2['B'])
    assert all(final_df['D'] == df2['D'])
    
def test_join_dfs_multiple_joins_3():
    new_keep_columns = deepcopy(keep_columns2)
    new_keep_columns['df1'].append('join')
    
    mdf = MergeDataframes(df_dict = deepcopy(df_dict),
                      base_df_name = 'df1',
                      df_relations = df_relations4,
                      keep_columns = new_keep_columns)

    mdf.merge_relations()
    final_df = mdf._base_df
    
    print(final_df.columns, final_df.shape)
    new_cols = ['A', 'join', 'D', 'H', 'equal', 'not_shared_join', 'df2_B', 'df1_B']
    assert final_df.shape == (size, len(new_cols))
    assert len(list(set.intersection(set(list(final_df.columns)),
                                     set(new_cols)))) == len(new_cols)
    assert all(final_df['equal'] == np.arange(size))
    assert all(final_df['A'] == df1['A'])
    assert all(final_df['join'] == df1['join'])
    assert all(final_df['df1_B'] == df1['B'])
    assert all(final_df['df2_B'] == df2['B'])
    assert all(final_df['D'] == df2['D'])

