import pandas as pd
import numpy as np

from timelord.table_timeseries import TableTimeSeriesTransformer

dates = pd.date_range('1/1/2000', periods=43200, freq='T')
nans = np.arange(43200.0)
nans[0:1000] = np.NaN

X = pd.DataFrame({'v_sum': np.arange(43200.0), 'v_avg': np.arange(43200), 'v_nans_sum': nans, 'v_nans_rem': nans, 'dates': dates})
date_column = 'dates'

def test_time_series_resample_3h():
    TTS = TableTimeSeriesTransformer(date_column = date_column, 
                                    min_freq = '3H', 
                                    operations = {'v_sum': 'sum', 'v_avg': 'mean', 'v_nans_sum': 'sum'})
    df_3h = TTS.fit_transform(X)

    assert df_3h.shape == (240, 3)
    assert df_3h.index[0].freq == '3H'
    assert df_3h['v_sum']['2000-01-01 00:00:00'] == 16110.0
    assert df_3h['v_avg']['2000-01-01 00:00:00'] == 89.5
    assert df_3h['v_nans_sum']['2000-01-01 00:00:00'] == 0.0
    assert df_3h['v_sum']['2000-01-01 18:00:00'] == df_3h['v_nans_sum']['2000-01-01 18:00:00']

def test_time_series_resample_3d():
    TTS = TableTimeSeriesTransformer(date_column = date_column, 
                                    min_freq = '3D', 
                                    operations = {'v_sum': 'sum', 'v_nans_sum': 'sum'})
    df_3d = TTS.fit_transform(X)

    assert df_3d.shape == (10, 2)
    assert df_3d.index[0].freq == '3D'
    assert df_3d['v_sum']['2000-01-01'] == 9329040.0
    assert df_3d['v_nans_sum']['2000-01-01'] == 8829540.0
    assert df_3d['v_sum']['2000-01-04'] == df_3d['v_nans_sum']['2000-01-04']
