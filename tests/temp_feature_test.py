import sys
#Change this path to your own location of the timelord directory

p = "/datadrive/casper_timelord/"
if p not in sys.path:
    sys.path.append(p)

import pandas as pd
from timelord.utils.util import Util
from timelord.lag_feature_generator import LagFeatureGenerator as tfg
from timelord.datasets import testdata


df = testdata.muscle_data()

def get_new_dates(df, date_col):
    #Transform non-datetime types to pandas datetime
    df[date_col] = df[date_col].apply(lambda x: pd.to_datetime(x, format ='%Y-%m-%d'))
    df = Util.recast_date_column(df, date_col)
    return df

# Create a dict containing all information necessary to generate temporal features for each dataframe.
date_column = 'date'
id_col = 'user'
period_lengths = [10]
column_operations = {
    'muscle_growth': ['max', 'median', 'min']
}

dynamic_dict = {}

dynamic_dict['df'] = {"df": df,
                            "date_col": date_column,
                            "id_col": id_col,
                            "period_lengths": period_lengths,
                            "col_operations": column_operations
                            }


#Create/Prepare the static data in the dataframe, in this case gender and eyecolor

#Get features from the static data
dummy_columns = ['sex', 'eyecolor']
#Get dummy features
dummy_data = pd.get_dummies(df[dummy_columns].fillna(0).astype('str')) #Gender has NaNs
df = df.drop(dummy_columns, axis=1)
#Get dates
dummy_data['date'] = df['date']
dummy_data['user'] = df['user']

#Not custom:
#Create a dictionary containing all dummy dataframes. They will be merged in the PredictionGenerator class

static_dict = {"df": dummy_data}

#Generate dictionaries containing the specifications for the merges.
#Change these arguments if you want an other kind of merge
dynamic_merge_args = {"how": 'left',
                      "on": 'user'}

static_merge_args = {"how": 'left',
                      "on": 'user'}

final_merge_args = {"how": 'left',
                    "on": 'user'}

max_date = pd.to_datetime(df['date'].max(), format="%Y-%m-%d")

temp_feat_gen = tfg(date_column, period_lengths, column_operations, id_col, max_date=None, njobs = 1)
fitted_temp_feat_gen = temp_feat_gen.fit(df)
temp_feats = fitted_temp_feat_gen.transform(df)



# period_df = temp_feat_gen.to_period_df(df, period_lengths[0], id_col, date_column)
# empty_df = temp_feat_gen.create_mock_df(df, id_col)
# temp_df = temp_feat_gen.period_to_temp_df(period_df, column_operations, period_lengths[0], id_col)

def test_create_temp_features():
    true_outcome = pd.DataFrame({'user': [0,1], 'muscle_growth_max_10': [46, 83],
                             'muscle_growth_median_10': [36.5, 67.0], 'muscle_growth_min_10': [24, 38]})
    true_outcome = true_outcome[['user', 'muscle_growth_max_10', 'muscle_growth_median_10', 'muscle_growth_min_10']]
    true_outcome = true_outcome.set_index('user', drop=True)
    temp_feat_gen = tfg(date_column, period_lengths, column_operations, id_col, max_date=None, njobs=1)
    fitted_temp_feat_gen = temp_feat_gen.fit(df)
    function_outcome = fitted_temp_feat_gen.transform(df)
    assert function_outcome.loc[function_outcome.index == 0].equals(true_outcome.loc[true_outcome.index == 0,:])
    assert function_outcome.loc[function_outcome.index == 1].equals(true_outcome.loc[true_outcome.index == 1,:])
#
# def test_to_period_df():
#     date_strings = ['2018-01-20', '2018-01-21', '2018-01-26', '2018-01-27', '2018-01-28', '2018-01-29',
#                                  '2018-01-20', '2018-01-21', '2018-01-26', '2018-01-27', '2018-01-28', '2018-01-29']
#     dates = pd.to_datetime(date_strings)
#     true_outcome = pd.DataFrame({'date': dates,
#                             'user': [0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1],
#                             'muscle_growth': [24, 37, 46, 36, 37, 33, 38, 50, 68, 83, 71, 66]})
#     true_outcome['user'] = true_outcome['user'].astype(int)
#     true_outcome = true_outcome[['date', 'user', 'muscle_growth']]
#     true_outcome.index = range(33,39) + range(13,19)
#     function_outcome = temp_feat_gen.to_period_df(df, period_lengths[0], id_col, date_column)
#     assert function_outcome.loc[function_outcome.user == 0].equals(true_outcome.loc[true_outcome.user == 0,:])
#     assert function_outcome.loc[function_outcome.user == 1].equals(true_outcome.loc[true_outcome.user == 1,:])
#
# def test_create_mock_df():
#    true_outcome = pd.DataFrame({'date': [0.0],
#                            'user': [1],
#                            'muscle_growth': [0.0]})
#    true_outcome = true_outcome[['date', 'user', 'muscle_growth']]
#    true_outcome['user'] = outcome['user'].astype(int)
#    function_outcome = temp_feat_gen.create_mock_df(df, id_col)
#    assert function_outcome.loc[function_outcome.user == 0].equals(true_outcome.loc[true_outcome.user == 0,:])
#    assert function_outcome.loc[function_outcome.user == 1].equals(true_outcome.loc[true_outcome.user == 1,:])
#
# def test_period_to_temp_df():
#     true_outcome = pd.DataFrame({'user': [0, 1], 'muscle_growth_max_10': [46, 83],
#                             'muscle_growth_median_10': [36.5, 67.0], 'muscle_growth_min_10': [24, 38]})
#     true_outcome = true_outcome[['user', 'muscle_growth_max_10', 'muscle_growth_median_10', 'muscle_growth_min_10']]
#     true_outcome = true_outcome.set_index('user')
#     function_outcome = temp_feat_gen.period_to_temp_df(period_df, column_operations, period_lengths[0], id_col)
#     assert function_outcome.loc[function_outcome.index == 0].equals(true_outcome.loc[true_outcome.index == 0,:])
#     assert function_outcome.loc[function_outcome.index == 1].equals(true_outcome.loc[true_outcome.index == 1,:])
