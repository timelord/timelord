Timelord documentation
======================
 
.. toctree::
    :maxdepth: 4
    :caption: Timelord
    :glob:

    timelord

.. toctree::
    :caption: Other

    license

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. mdinclude:: ../../README.md