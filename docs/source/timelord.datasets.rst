timelord\.datasets package
==========================

Submodules
----------

timelord\.datasets\.load\_data module
-------------------------------------

.. automodule:: timelord.datasets.load_data
    :members:
    :undoc-members:
    :show-inheritance:

timelord\.datasets\.testdata module
-----------------------------------

.. automodule:: timelord.datasets.testdata
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: timelord.datasets
    :members:
    :undoc-members:
    :show-inheritance:
