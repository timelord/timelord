# Timelord's documentation
We document the Timelord project using [Sphinx documentation][sphinx-docs]. This allows us to automatically build the documentation from source docstrings. The documenation is built and published on [readthedocs.io][readthedocs].

Below, we will describe the mechanisms behind this process and how you can replicate this yourself.

# Using documentation:
If you have new .py-files, or editted, please run the following command from the `docs` folder:
```
make rst
```

To see how the docs look like, run this:

```
make html
```

The HTML that is built, can be found in `docs/build`.

# Contents
- [Timelord's documentation](#timelords-documentation)
- [Contents](#contents)
- [Getting the tools](#getting-the-tools)
- [Build process](#build-process)
    - [Configuration](#configuration)
    - [Python modules](#python-modules)
        - [Autodoc](#autodoc)
        - [Napoleon](#napoleon)
    - [sphinx-apidoc](#sphinx-apidoc)
- [Read the Docs](#read-the-docs)

# Getting the tools
Sphinx is a Python-based tool, installation instructions can be found [here][sphinx-install].

# Build process
Sphinx builds documentation based on _reStructuredText_. The documentation is initiated from `timelord/docs` The source-files for the build are found in `timelord/docs/source`, the build-output is stored in `timelord/docs/build`.

## Configuration
Sphinx's configuration is defined in the `timelord/docs/source/conf.py`.

## Python modules
In order to build proper documentation, we make use of a couple Sphinx-extensions. Most notably `autodoc` and `napoleon`.

### Autodoc
The [`autodoc`-extension][autodoc-extension] allows for automatic documentation of the docstrings within python modules. For this to work, it is important that docstrings within the modules are properly formatted. As Sphinx builds documentation based on _reStructuredText_, this is the default expected format. More on this [later](###Napoleon).

### Napoleon
For Timelord, we have chosen to write docstrings in numpy-format. This is easier to read and write than pure _reStructuredText_. However, it is not supported by Sphinx in its default config. The [`napoleon`-extension][napoleon-extension] enables Sphinx to parse either numpy- or Google-style formatted docstring to _reStructuredText_.

### m2r
For converting the README.md to _reStructuredText_, we use the m2r library. This makes sure that the index page of the documentation is the same as the Readme.

## sphinx-apidoc
Generates source-files for autodoc in the `timelord/docs/source/` folder. Each file needs to be generated once for every module.

# Read the Docs


[sphinx-docs]: http://www.sphinx-doc.org/en/master/
[readthedocs]: http://timelord.readthedocs.io/en/latest/
[sphinx-install]: http://www.sphinx-doc.org/en/master/usage/installation.html
[autodoc-extension]: http://www.sphinx-doc.org/en/master/ext/autodoc.html
[napoleon-extension]: http://www.sphinx-doc.org/en/master/ext/napoleon.html